"""
Author Aurore Guillevic
email  aurore.guillevic@inria.fr
date   April 14, 2021

example of index calculus in (Z/pZ)*
"""
from sage.rings.fast_arith import prime_range
from sage.misc.misc_c import prod
from sage.arith.misc import is_prime
from sage.arith.misc import factor
from sage.arith.misc import GCD, gcd
from sage.matrix.constructor import Matrix, matrix
from sage.rings.finite_rings.finite_field_constructor import FiniteField, GF
from sage.rings.integer_ring import Z, ZZ

from pretty_print import pretty_print_product, pretty_print_tex_product

def factor_B_smooth_part(n, prod_primes):
    """return B-smooth part and non-factorized part.
    B-smooth means only factors in prod_primes.
    """
    m = n
    fac = 1
    c = prod_primes
    while c > 1:
        c = gcd(m, c)
        m = m // c
        fac = fac * c
    return fac, m

def index_calculus(p, l, B, target, with_tex_slides=False):
    """Basic index calculus in GF(p), l | (p-1), smoothness bound B

    INPUT:
    - `p`: prime
    - `l`: prime, l | (p-1)
    - `B`: smoothness bound
    """
    assert is_prime(p)
    assert ((p-1) % l) == 0
    assert is_prime(l)
    cofactor = (p-1)//l
    Fl = GF(l)
    # if p = 3 mod 4 and l = (p-1)/2 is prime:
    # because (p-1)/2 is prime, there is a subgroup of order 2: {1,-1} and a subgroup of order l,
    # and any number not 1, not -1, (not 0), is a generator of the subgroup of order l.
    # moreover if g^l = -1, then g is a generator of Fp*, otherwise (g^l = 1) then (-g) is.
    fac = factor(cofactor)
    subgroups = [(p-1)//pi for pi, ei in fac]
    Fp = GF(p)
    g = Fp(2)
    while len([1 for li in subgroups if g**li == 1]) > 0:
        g = g+1
    h = Fp(2)
    while h**l != 1:
        h = h+1
    print("p = {} l = (p-1)/{} = {}".format(p, cofactor, l))
    print("generator of Fp* g = {}, of subgroup of order l: h = {}".format(g, h))
    gen = g # one can take also gen = h
    factor_basis = list(prime_range(B+1))
    prime_to_index = {factor_basis[i]: i for i in range(len(factor_basis))}
    prod_primes = prod(factor_basis)
    number_rels = 0
    ei = 1
    list_rows = []
    column_vector = []
    if with_tex_slides:
        str_tex_rels = ""
    while ei < l and number_rels < len(factor_basis):
        n = ZZ(gen**ei)
        smooth_n, hard = factor_B_smooth_part(n, prod_primes)
        if hard == 1:
            assert smooth_n == n
            new_row = [0]*len(factor_basis)
            for pj, ej in n.factor():
                new_row[prime_to_index[pj]] = ej
            one_more_rel = False
            if number_rels == 0:
                list_rows = [new_row]
                column_vector = [ei]
                one_more_rel = True
                if with_tex_slides:
                    str_tex_rels += "g^{\\mycolor{"+"{:3d}".format(ei)+"}}}}  & {:4d} & {} \\\\".format(n, " \\cd ".join([str(bj) if ej == 1 else str(bj)+"^{"+str(ej)+"}" for bj,ej in smooth_n.factor()]))
            else:
                mat = Matrix(number_rels+1, len(factor_basis), list_rows + [new_row])
                if mat.rank() == number_rels+1:
                    list_rows.append(new_row)
                    column_vector.append(ei)
                    one_more_rel = True
                    if with_tex_slides:
                        str_tex_rels += "\ng^{\\mycolor{"+"{:3d}".format(ei)+"}}}}  & {:4d} & {} \\\\".format(n, " \\cd ".join([str(bj) if ej == 1 else str(bj)+"^{"+str(ej)+"}" for bj,ej in smooth_n.factor()]))
            if one_more_rel:
                print("{}^{:3d} = {:4d} = {}".format(gen, ei, n, smooth_n.factor()))
                number_rels += 1
        ei += 1
    max_ei = ei-1

    if number_rels >= len(factor_basis):
        print("enough relations: {}, factor_basis: {} length {}".format(number_rels, factor_basis, len(factor_basis)))
        mat = Matrix(Fl, number_rels, len(factor_basis), list_rows)
        print(mat)
        vec = Matrix(Fl, number_rels, 1, column_vector)
        ker = mat.solve_right(vec)
        vec_ker = [ker[i][0] for i in range(number_rels)]
        print("solve M*x = V: x = {}".format(vec_ker))
        i = 0
        for log_pi in vec_ker:
            ni = ZZ(Fp(factor_basis[i])/gen**log_pi)
            if abs(ni-p) < ni:
                ni = ni-p
            print("{}/{}^{} = {}".format(factor_basis[i], gen, log_pi, ni))
            i += 1
    print("")

    if with_tex_slides:
        print("")
        print("\\newcommand{\\mycolor}[1]{\\vlt{#1}}")
        print("\\begin{frame}{Index calculus in $(\\Z/p\\Z)^{*}$: example}")
        print("$p = {}$, $\\ell = (p-1)/{} = {}$ prime \\\\".format(p, cofactor, l))
        print("Smoothness bound $B={}$ \\\\".format(B))
        print("$\\mathcal{{F}}_{{{}}} = \\{{{}\\}}$ small primes up to $B$, $\\# \\cF = {}$\\\\".format(B, ",".join([str(bi) for bi in factor_basis]), len(factor_basis)))
        print("\\bblue{$B$-smooth integer}:")
        print("$n = {}^{{e_1}} {}^{{e_2}} \\cdots {}^{{e_{{{}}}}}$, $e_i \\geq 0$ \\\\".format(factor_basis[0], factor_basis[1], factor_basis[-1], len(factor_basis)))
        print("is $g^s \\bmod p$ smooth? $1 \\leq s \\leq {} $ is enough \\\\".format(max_ei))
        print("\\begin{tabular}{@{}l@{}l@{}}")
        print("& \\begin{scriptsize}\\begin{tabular}{"+"p{1.8em}@{}"*len(factor_basis)+"} "+"&".join([str(bi) for bi in factor_basis])+"\\end{tabular} \\end{scriptsize}\\\\")
        print("$\\begin{array}{@{}l@{\\,\\equiv\\,}r@{\\,=\\,}l@{}}")
        print(str_tex_rels)
        print("\\end{array}")
        print("\\to $ & ")
        print("$\\begin{bmatrix}")
        for row in list_rows:
            print("  "+" & ".join([str(ei) if ei > 0 else " " for ei in row])+" \\\\")
        print("\\end{bmatrix}")
        print("\\cdot \\vec{x} = ")
        print("\\begin{bmatrix}")
        print(" \\\\ ".join(["\\mycolor{{{}}}".format(ei) for ei in column_vector]) + " \\\\")
        print("\\end{bmatrix}$")
        print("\\end{tabular}\\\\")

        if number_rels >= len(factor_basis):
            print("\\vspace{\\baselineskip}")
            print("$\\vec{x} = "+str(vec_ker) + " \\bmod " + str(l) + "$ \\\\")
            colors = ["blue", "violet", "orange", "purple", "magenta", "green", "bblue"]
            pi_color = {factor_basis[i]: colors[i % len(colors)] for i in range(len(factor_basis))} 

            logs_mod_p_1 = [ZZ(vi) for vi in vec_ker]
            # find subgroup elements, where cofactor = (p-1)//l
            gen_co = g**l
            elt_to_log_cofactor = {gen_co**ei: ei for ei in range(0, cofactor)}
            #print("elt_to_log_cofactor = {}".format(elt_to_log_cofactor))
            log_pi = ZZ(vec_ker[0])
            mi = gen**log_pi
            #print("mi = {}^{} = {}, mi/{} = {}".format(gen, log_pi, mi, factor_basis[0], mi/factor_basis[0]))
            log_pi_ = log_pi - l*elt_to_log_cofactor[mi/factor_basis[0]]
            if log_pi_ < 0:
                log_pi_ += p-1
            logs_mod_p_1[0] = log_pi_
            mi = ZZ(mi/factor_basis[0])
            if abs(mi-p) < mi:
                mi = mi-p
            ni = ZZ(gen**log_pi_)
            if abs(ni-p) < ni:
                ni = ni-p
            if mi == 1:
                str_log = "${}^{{{}}} = {}".format(gen, log_pi, factor_basis[0])
            elif mi == -1:
                str_log = "${}^{{{}}} = -{}".format(gen, log_pi, factor_basis[0])
            else:
                str_log = "${}^{{{}}} = {} \\cdot {}".format(gen, log_pi, mi, factor_basis[0])
            str_log_p_1 = "${}^{{{}}} = \\{}{{{}}}".format(gen, log_pi_, pi_color[ni], ni)
            i = 1
            for log_pi in vec_ker[1:]:
                mi = gen**log_pi
                #print("mi = {}^{} = {}, mi/{} = {}".format(gen, log_pi, mi, factor_basis[i], mi/factor_basis[i]))
                log_pi_ = ZZ(log_pi) - l*elt_to_log_cofactor[mi/factor_basis[i]]
                if log_pi_ < 0:
                    log_pi_ += p-1
                logs_mod_p_1[i] = log_pi_
                mi = ZZ(mi/factor_basis[i])
                if abs(mi-p) < mi:
                    mi = mi-p
                ni = ZZ(gen**log_pi_)
                if abs(ni-p) < ni:
                    ni = ni-p
                if mi == 1:
                    str_log += ", {}^{{{}}} = {}".format(gen, log_pi, factor_basis[i])
                elif mi == -1:
                    str_log += ", {}^{{{}}} = -{}".format(gen, log_pi, factor_basis[i])
                else:
                    str_log += ", {}^{{{}}} = {} \\cdot {}".format(gen, log_pi, mi, factor_basis[i])
                str_log_p_1 += ", {}^{{{}}} = \\{}{{{}}}".format(gen, log_pi_, pi_color[ni], ni)
                i = i+1
            str_log += "$"
            str_log_p_1 += "$"

            print(str_log + " \\\\")
            print("\\end{frame}\n")

            print("\\begin{frame}{Index calculus in $(\\Z/p\\Z)^{*}$: example}")
            print("$\\vec{x} = "+str(vec_ker) + " \\bmod " + str(l) + "$ \\\\")
            print("$\\vec{v} = "+str(logs_mod_p_1) + " \\bmod p-1 = " + str(p-1) + "$ \\\\")
            print(str_log_p_1 + " \\\\")

            # target
            print("Target $h = {}$, ".format(target))
            ei = 0
            t = Fp(target)
            smooth_part, hard_part = factor_B_smooth_part(ZZ(t), prod_primes)
            while abs(hard_part) > 1:
                t *= gen
                ei += 1
                smooth_part, hard_part = factor_B_smooth_part(ZZ(t), prod_primes)
            fac_target = factor(ZZ(t))
            str_target = "$g^{\\red{" +str(ei) + "}} \\cd "+str(target)+" = "+str(ZZ(t))+" = "
            i = 0
            for tj, ej in fac_target:
                str_target += "\\" + pi_color[tj] + "{" + str(tj)
                if ej > 1:
                    str_target += "^{"+str(ej)+"}"
                str_target += "}"
                i = i+1
                if i < len(fac_target):
                    str_target += " \\cd "
                else:
                    str_target += "\\bmod p$ \\\\"
            print(str_target)
            # now final count:
            #$\log_2 777 = \red{-10} + \blue{2 \log_g 3} + \violet{\log_g 5} + \orange{\log_g 11} = 824 \bmod p-1$ \\
            #$g^{824} = 777$
            str_target = "$\\log_{{{}}} {} = \\red{{-{}}}".format(gen, target, ei)
            for tj, ej in fac_target:
                str_target += " + \\" + pi_color[tj] + "{"
                if ej > 1:
                    str_target += str(ej)
                str_target += "\\log_{"+str(gen) + "} " + str(tj)+"}"
                i = i+1
            pi_to_idx = {factor_basis[i]: i for i in range(len(factor_basis))}
            #pi logs_mod_p_1
            log_target = (p-1 -ei + sum([ej * logs_mod_p_1[pi_to_idx[int(tj)]] for tj, ej in fac_target])) % (p-1)
            str_target += " = {} \\bmod p-1$ \\\\".format(log_target)
            print(str_target)
            print("${}^{{{}}} = {} \\bmod p$ \\\\".format(gen, log_target, gen**log_target))
        print("\\end{frame}")
        print("")

    
if __name__ == "__main__":
    # print([p for p in prime_range(2000, 20000) if (p % 4) == 3 and is_prime((p-1)//2)])
    p = 1109; index_calculus(p, (p-1)//4, 13, 777, with_tex_slides=True)
    #p = 2027; index_calculus(p, (p-1)//2, 13, 314)
    p = 2039; index_calculus(p, (p-1)//2, 17, 314, with_tex_slides=True)
    #p = 2099; index_calculus(p, (p-1)//2, 17, 314, with_tex_slides=True)
    #p = 2459; index_calculus(p, (p-1)//2, 19, 314)
    #p = 2579; index_calculus(p, (p-1)//2, 23, 314)
