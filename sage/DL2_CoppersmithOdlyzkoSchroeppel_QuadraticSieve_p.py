"""
Author Aurore Guillevic
email  aurore.guillevic@inria.fr
date   April 14, 2021

example of Coppersmith Odlyzko Schroeppel DL computation algorithm in (Z/pZ)*
@Article{CopOdlSch86,
  author    = {Don Coppersmith and Andrew M. Odlyzko and Richard Schroeppel},
  title     = {Discrete logarithms in {GF}$(p)$},
  journal   = {Algorithmica},
  volume    = {1},
  number    = {1},
  pages     = {1-15},
  year      = {1986},
  publisher = {Springer},
  doi       = {10.1007/BF01840433},
  note      = {url{https://dl.acm.org/citation.cfm?id=6835}},
}

if p = 3 mod 8, f = x^2 + 2, class number 1, unit -1
  (Weber Denny record computation in 1995)
if p = 7 mod 8, f = x^2 - 2, class number 1, fundamental unit x+1
if p = 1 mod 4, (p-1)/2 is not prime, l = (p-1)/4, and f = x^2 + 1
"""

from sage.rings.fast_arith import prime_range
from sage.misc.misc_c import prod
from sage.arith.misc import is_prime
from sage.arith.misc import factor
from sage.arith.misc import GCD, gcd
from sage.matrix.constructor import Matrix, matrix
from sage.rings.finite_rings.finite_field_constructor import FiniteField, GF
from sage.rings.integer_ring import Z, ZZ
from sage.rings.rational_field import Q, QQ
from sage.rings.number_field.number_field import NumberField

from pretty_print import pretty_print_product, pretty_print_tex_product

def factor_B_smooth_part(n, prod_primes):
    """return B-smooth part and non-factorized part.
    B-smooth means only factors in prod_primes.
    """
    m = n
    fac = 1
    c = prod_primes
    while c > 1:
        c = gcd(m, c)
        m = m // c
        fac = fac * c
    return fac, m

def collect_relations_naive(A, B, f0_bivariate, f1_bivariate, Kf1, factor_basis_f0, factor_basis_f1, prime_f0_to_idx, ideal_f1_to_idx):
    prod_primes = prod(prime_range(B+1))
    (s,) = Kf1._first_ngens(1)
    units_Kf1 = Kf1.units()
    with_units = len(units_Kf1) > 0
    if with_units:
        if 1 not in factor_basis_f1:
            factor_basis_f1[1] = [ui for ui in units_Kf1]
        idx = len(prime_f0_to_idx) + len(ideal_f1_to_idx)
        for ui in units_Kf1:
            if ui not in ideal_f1_to_idx:
                ideal_f1_to_idx[ui] = idx
                idx += 1
    row_id = 0
    #cols = len(factor_basis_f0) + sum([len(factor_basis_f1[qi]) for qi in factor_basis_f1])
    cols = len(prime_f0_to_idx) + len(ideal_f1_to_idx)
    rows = cols
    empty_row = [0]*cols
    u = f0_bivariate([0, 1])
    v = f0_bivariate([1, 0])
    if abs(v) > 1: # and abs(v) not in factor_basis_f0:
        empty_row[prime_f0_to_idx[v]] = -1
    mat = [[j for j in empty_row] for i in range(rows)]
    # actually, finding a relation with v corresponds to a relation with (a,b) = (1,0) and f1 being monic
    #if abs(v) > 1 and abs(v) not in factor_basis_f0:
    #    # if v is B-smooth: add a relation to cancel the column of v
    #    smooth_part_v, hard_v = factor_B_smooth_part(v, prod_primes)
    #    if abs(hard_v) == 1: # v is B-smooth
    #        factors_v = factor(v)
    #        for (q, ei) in factors_v:
    #            mat[row_id][prime_f0_to_idx[q]] += ei
    #        print("{:3},{:2} & {: 4} & {:13s} & {:3d} & {:15s} & {:33s} & {} \\\\".format(1, 0, v, pretty_print_tex_product(v, factors_v), 0, "0", "0", ""))
    #        row_id += 1
    
    for a in range(-A,A+1):
        for b in [b for b in range(0,A+1) if gcd(a,b) == 1]:
            nf0 = f0_bivariate([a, b])
            nf1 = f1_bivariate([a, b])
            if nf1 == 0 or nf0 == 0:
                continue
            smooth_part_nf0, hard_nf0 = factor_B_smooth_part(nf0, prod_primes)
            smooth_part_nf1, hard_nf1 = factor_B_smooth_part(nf1, prod_primes)
            if abs(hard_nf0) != 1 or abs(hard_nf1) != 1:
                continue
            if row_id >= rows:
                # add an empty row
                mat.append([j for j in empty_row])
            # nf1 and nf0 are both smooth
            str_f1 = ""
            prod_ideal_f1 = Kf1(1)
            factors_f1 = factor(nf1)
            for (q, ei) in factors_f1:
                Fq = GF(q)
                #Fqz.<z> = Fq[]
                Fqz = Fq['z']; (z,) = Fqz._first_ngens(1)
                ideal_f1 = gcd(f1_bivariate([z,1]), a-b*z) # or a+b*z ???
                for ideal_s in factor_basis_f1[q]:
                    if gcd(Fqz(ideal_s.polynomial()), ideal_f1) > 1:
                        str_f1 += "("+str(q)+","+str(ideal_s)+")"
                        if ei > 1:
                            str_f1 += "^"+str(ei)
                        prod_ideal_f1 *= ideal_s**ei
                        try:
                            mat[row_id][ideal_f1_to_idx[ideal_s]] = -ei # everything negative (-) on the algebraic side
                        except IndexError as err:
                            print("error row_id = {}, len(mat) = {}, ideal_f1_to_idx[{}] = {}, len(empty_row) = {}".format(row_id, len(mat), ideal_s, ideal_f1_to_idx[ideal_s], len(empty_row)))
                        break # break because there is only one ideal (to some power) per prime q
            unit_f1 = (a-b*s)/prod_ideal_f1 # unit, can be just -1
            # now decompose the unit over the fundamental units if needed
            if unit_f1 != 1 and unit_f1 != -1 and with_units and len(units_Kf1) == 1:
                ei = 1
                unit = units_Kf1[0]
                while unit**ei != unit_f1 and unit**ei != -unit_f1:
                    if ei > 0:
                        ei = -ei
                    else:
                        ei = -ei+1
                #print("unit = {}, unit_f1 = {}, ({})^{} = {}".format(unit, unit_f1, unit, ei, unit**ei))
                mat[row_id][ideal_f1_to_idx[unit]] = -ei
            factors_f0 = factor(nf0)
            for (q, ei) in factors_f0:
                mat[row_id][prime_f0_to_idx[q]] += ei # + because in case this is already the column for 1/v (leading coefficient of v*x-u)
            print("{:3},{:2} & {: 4} & {:13s} & {:3d} & {:15s} & {:33s} & {} \\\\".format(a, b, nf0, pretty_print_tex_product(nf0, factors_f0), nf1, pretty_print_tex_product(nf1, factors_f1), str_f1, unit_f1))
            row_id += 1
    return mat

def COS(p, B, A):
    """
    Compute DL

    INPUT:
    - `p`: prime
    - `B`: smoothness bound
    - `A`: relation collection bound
    """
    # preparse("QQx.<x> = QQ[]")
    ZZx = ZZ['x']; (x,) = ZZx._first_ngens(1)
    # preparse("QQXY.<X,Y> = QQ[]")
    ZZXY = ZZ['X, Y']; (X, Y,) = ZZXY._first_ngens(2)
    
    if (p % 4) == 1:
        f1 = x**2 + 1
        f1_bivariate = X**2 + Y**2
        l = (p-1)//4
        subgroup_cofactor = 4
    elif (p % 8) == 3:
        f1 = x**2 + 2
        f1_bivariate = X**2 + 2*Y**2
        l = (p-1)//2
        subgroup_cofactor = 2
    elif (p % 8) == 7:
        f1 = x**2 - 2 # but there is a unit...
        f1_bivariate = X**2 - 2*Y**2
        l = (p-1)//2
        subgroup_cofactor = 2
    assert is_prime(p) and is_prime(l)
    Fl = GF(l)
    print("p = {}".format(p))
    print("(p-1)/{} = {}".format(subgroup_cofactor, l))
    Fp = GF(p)
    # compute the roots of f mod p
    roots_mod_p = f1.roots(Fp)
    root = ZZ(roots_mod_p[0][0])
    M = Matrix(ZZ, 2, 2, [p, 0, root, 1])
    R = M.LLL()
    u = abs(R[0][0])
    v = abs(R[0][1])
    try:
        assert abs(f1_bivariate([u,v])) == p
    except AssertionError as err:
        print("f_bivariate([u,v]) = {}, p = {}".format(f1_bivariate([u,v]), p))
        u = abs(R[1][0])
        v = abs(R[1][1])
        try:
            assert abs(f1_bivariate([u,v])) == p
        except AssertionError as err:
            print("f_bivariate([u,v]) = {}, p = {}".format(f1_bivariate([u,v]), p))
            raise err
    f1_cst = ZZ(f1.constant_coefficient())
    if f1_cst == 1:
        print("{}^2 + {}^2 = {}, p = {}".format(u,v, u**2+v**2, p))
    else:
        print("{}^2 {:+}*{}^2 = {}, p = {}".format(u, f1_cst, v, u**2 + f1_cst*v**2, p))
    assert (Fp(u/v))**2 == Fp(-f1_cst)
    # linear side: polynomial of degree 1
    f0 = v*x - u
    f0_bivariate = v*X -u*Y
    print("f1 = {} f0 = {}".format(f1, f0))

    # generate factor bases
    factor_basis_f0 = list(prime_range(B+1))
    prime_f0_to_idx = {factor_basis_f0[i]: i for i in range(len(factor_basis_f0))}
    idx_ideal = len(factor_basis_f0)
    if abs(v) > B:
        prime_f0_to_idx[v] = len(factor_basis_f0)
        idx_ideal += 1
    ideal_f1_to_idx = {}
    factor_basis_f1 = {}
    #Kf1.<s> = NumberField(f1)
    #preparse("Kf1.<s> = NumberField(f1)")
    Kf1 = NumberField(f1, names=('s',)); (s,) = Kf1._first_ngens(1)
    print("factor basis for f1 = {}, B = {}".format(f1, B))
    for q in prime_range(B+1):
        Fq = GF(q)
        # Fqz.<z> = Fq[]
        # preparse("Fqz.<z> = Fq[]")
        Fqz = Fq['z']; (z,) = Fqz._first_ngens(1)
        f1q = Fqz(f1)
        roots_q = f1q.roots()
        if len(roots_q) > 0:
            for r,ei in roots_q:
                r = ZZ(r)
                if abs(r-q) < r:
                    r = r-q
                if f1_bivariate([r, 1]) == q:
                    a,b = r, 1
                else:
                    M = Matrix(ZZ, 2, 2, [q, 0, r, 1])
                    R = M.LLL()
                    a0 = R[0][0]; b0 = R[0][1]
                    a1 = R[1][0]; b1 = R[1][1]
                    if abs(f1_bivariate([a0, b0])) == q:
                        a, b = a0, b0
                    elif abs(f1_bivariate([a1, b1])) == q:
                        a, b = a1, b1
                    else:
                        bool_, gen_q = (Kf1.ideal([q, s-r])).is_principal()
                        if bool_:
                            a,b = gen_q.polynomial().list()
                        else:
                            a,b = r, 1
                            print("no generator for ideal <{}, {}> above {}".format(q, s-r, q))
                if b < 0:
                    a,b = -a, -b
                if q in factor_basis_f1:
                    factor_basis_f1[q].append(-a+b*s)
                else:
                    factor_basis_f1[q] = [-a+b*s]
                ideal_f1_to_idx[-a+b*s] = idx_ideal
                idx_ideal += 1
            print("{}: {}".format(q, (f1q).factor()))

    print("factor_basis_f0 = {}".format(factor_basis_f0))
    print("factor_basis_f1 = {}".format(factor_basis_f1))
    print("prime_f0_to_idx = {}".format(prime_f0_to_idx))
    print("ideal_f1_to_idx = {}".format(ideal_f1_to_idx))
    cols = len(prime_f0_to_idx) + len(ideal_f1_to_idx)
    Kf1_units = Kf1.units()
    print("has K fundamental units? {}, {}".format(len(Kf1_units) > 0, Kf1_units))
    factor_basis_f1[1] = [ui for ui in Kf1_units]
    for ui in Kf1_units:
        ideal_f1_to_idx[ui] = idx_ideal
        idx_ideal += 1
    #rows = cols + 1 + len(Kf1_units)

    # without sieve:
    mat = collect_relations_naive(A, B, f0_bivariate, f1_bivariate, Kf1, factor_basis_f0, factor_basis_f1, prime_f0_to_idx, ideal_f1_to_idx)
    #print(mat)
    matZ = Matrix(ZZ, mat)
    matFl = Matrix(Fl, mat)
    print(matZ)
    print(matFl)
    ker_right = matFl.right_kernel()
    print(ker_right)
    

if __name__ == "__main__":
    # print([p for p in prime_range(1000, 20000) if (p % 8) == 3 and is_prime((p-1)//2)])
    list_p = [1019, 1187, 1283, 1307, 1523, 1619, 1907, 2027, 2099, 2459, 2579, 2819, 2963, 3203, 3467, 3779, 3803, 3947, 4139, 4259, 4283, 4547, 4787, 5099, 5387, 5483, 5507, 5939, 6659, 6779, 6827, 6899, 7187, 7523, 7643, 8147, 8699, 8747, 8819, 8963, 9467, 9587, 10163, 10667, 10883, 11003, 11483, 11699, 12107, 12203, 12227, 12347, 12539, 12659, 12899, 13043, 13163, 13523, 14243, 14387, 14699, 14867, 15083, 15299, 15683, 15803, 16139, 16187, 16547, 17027, 17387, 17483, 17939, 18059, 18443, 18587, 18947, 19259, 19379]
    # print([p for p in prime_range(2000, 20000) if (p % 8) == 7 and is_prime((p-1)//2)])
    # [2039, 2063, 2207, 2447, 2879, 2903, 2999, 3023, 3119, 3167, 3623, 3863, 4007, 4079, 4127, 4679, 4703, 4799, 4919, 5087, 5399, 5639, 5807, 5879, 5927, 6047, 6599, 6719, 6983, 7079, 7247, 7559, 7607, 7703, 7727, 7823, 8039, 8423, 8543, 8783, 9743, 9839, 9887, 10007, 10079, 10103, 10343, 10463, 10559, 10607, 10799, 11279, 11423, 11807, 12263, 12527, 12647, 12983, 13103, 13127, 13799, 13967, 14087, 14159, 14207, 14303, 14423, 15287, 15383, 15647, 15767, 16223, 16487, 17327, 17903, 18119, 18743, 18839, 18959, 19079, 19583]
    # print([p for p in prime_range(1000, 20000) if (p % 8) == 5 and is_prime((p-1)//4)])
    # [1109, 1229, 1493, 1637, 1733, 1949, 1997, 2309, 2477, 2693, 2837, 2909, 2957, 3413, 3533, 3677, 3989, 4133, 4157, 4253, 4349, 4373, 4493, 4517, 5189, 5309, 5693, 5717, 5813, 6173, 6197, 6269, 6317, 6389, 6653, 7013, 7109, 7517, 7949, 8069, 8117, 8573, 9173, 9533, 9749, 10589, 10709, 10733, 10853, 11069, 11213, 11549, 11813, 12149, 12197, 12269, 12437, 12653, 12917, 13037, 13229, 13829, 13877, 13997, 14549, 14957, 15077, 15173, 15413, 15629, 15773, 16229, 17093, 17189, 17789, 18077, 18269, 18413, 18917, 19037, 19157, 19949, 19973, 19997]

    min_uv = max(list_p)
    for p in list_p:
        Fp = GF(p)
        s = ZZ((Fp(-2)).sqrt())
        M = Matrix(2, 2, [p, 0, s, 1])
        R = M.LLL()
        u = abs(R[0][0])
        v= abs(R[0][1])
        assert u**2+2*v**2 == p
        print("p = {:5d} = {:3d}^2 + 2 *{:3d}^2".format(p, u, v))
        max_uv = max(u,v)
        if max_uv < min_uv:
            min_uv = max_uv
            p_min_uv = p
            min_u_v = (u, v)
    print("min u, v = {}, {} for p = {}".format(min_u_v[0], min_u_v[1], p_min_uv))

    
    #COS(2027, 13, 21)
    COS(2039, 13, 21)
    #COS(1109, 13, 21)
    #COS(2099, 13, 21)
