def pretty_print_product(n, fac_n):
    """print without white spaces"""
    if len(fac_n) == 0:
        return str(n)
    if n < 0:
        str_fac_n="-"
    else:
        str_fac_n=""
    for (fi,ei) in fac_n[:-1]:
        str_fac_n += str(fi)
        if ei > 1:
            str_fac_n += "^"+str(ei)
        str_fac_n += "*"
    (fi,ei) = fac_n[-1]
    str_fac_n += str(fi)
    if ei > 1:
        str_fac_n += "^"+str(ei)
    return str_fac_n

def pretty_print_tex_product(n, fac_n):
    """print without white spaces and with \cdot for *"""
    if len(fac_n) == 0:
        return str(n)
    if n < 0:
        str_fac_n="-"
    else:
        str_fac_n=""
    for (fi,ei) in fac_n[:-1]:
        str_fac_n += str(fi)
        if ei >= 10:
            str_fac_n += "^{"+str(ei)+"}"
        elif ei > 1:
            str_fac_n += "^"+str(ei)
        str_fac_n += "\cd "
    (fi,ei) = fac_n[-1]
    str_fac_n += str(fi)
    if ei >= 10:
        str_fac_n += "^{"+str(ei)+"}"
    elif ei > 1:
        str_fac_n += "^"+str(ei)
    return str_fac_n

def pretty_print_units(unit_exp, unit_sign, str_u):
    #-uf^{-11}
    if unit_exp == 0:
        str_u = unit_sign+"1       "
    else:
        str_u = unit_sign+"u"+str_u
        if unit_exp < 0:
            str_u += "^{"+str(unit_exp)+"}"
            if unit_exp > -10:
                str_u += " "
        elif unit_exp > 1:
            if unit_exp >= 10:
                str_u += "^{"+str(unit_exp)+"} "
            else:
                str_u += "^"+str(unit_exp)+"    "
        else:
            str_u += "      "
    return str_u


def nice_latex_matrix_print_color(M, q):
    """ if M[i][j] % q == 0, print cell in color"""
    s = ""
    A = -(M.nrows()//2)
    colored_cols = [False for j in range(M.ncols())]
    for j in range(M.ncols()):
        i = 0
        while i < M.nrows() and ((M[i][j] == 0) or ((M[i][j] % q) != 0)):
            i += 1
        colored_cols[j] = (i < M.nrows())

    for i in range(M.nrows()):
        s += "{:3d}".format(A)
        A += 1
        for j in range(M.ncols()):
            if not colored_cols[j]:
                if M[i][j] == 0:
                    s += "&    "
                else:
                    s += "&{:4d}".format(M[i][j])
            else:
                if M[i][j] == 0:
                    s += "&         "
                elif (M[i][j] % q) != 0:
                    s += "&    {:4d} ".format(M[i][j])
                else:
                    s += "&\\cl{{{:4d}}}".format(M[i][j])
        s += " \\\\\n"
    return s

def nice_latex_matrix_print_color_update(M, M1, q):
    """ if M[i][j] % q == 0, print cell M1[i][j] in color"""
    s = ""
    A = -(M.nrows()//2)
    colored_cols = [False for j in range(M.ncols())]
    for j in range(M.ncols()):
        i = 0
        while i < M.nrows() and ((M[i][j] == 0) or ((M[i][j] % q) != 0)):
            i += 1
        colored_cols[j] = (i < M.nrows())

    for i in range(M.nrows()):
        s += "{:3d}".format(A)
        A += 1
        for j in range(M.ncols()):
            if not colored_cols[j]:
                if M1[i][j] == 0:
                    s += "&    "
                else:
                    s += "&{:4d}".format(M1[i][j])
            else:
                if M[i][j] == 0:
                    s += "&         "
                elif (M[i][j] % q) != 0:
                    s += "&    {:4d} ".format(M1[i][j])
                else:
                    s += "&\\CL{{{:4d}}}".format(M1[i][j])
        s += " \\\\\n"
    return s

def nice_latex_matrix_print_color_1(M):
    """ if M[i][j] == +/-1, print cell M1[i][j] in color"""
    s = ""
    A = -(M.nrows()//2)
    colored_cols = [False for j in range(M.ncols())]
    for j in range(M.ncols()):
        i = 0
        while i < M.nrows() and ((M[i][j] != 1) and (M[i][j] != -1)):
            i += 1
        colored_cols[j] = (i < M.nrows())

    for i in range(M.nrows()):
        s += "{:3d}".format(A)
        A += 1
        for j in range(M.ncols()):
            if not colored_cols[j]:
                if M1[i][j] == 0:
                    s += "&    "
                else:
                    s += "&{:4d}".format(M[i][j])
            else:
                if M[i][j] == 0:
                    s += "&      "
                elif (M[i][j] != 1 and M[i][j] != -1):
                    s += "& {:4d} ".format(M[i][j])
                else:
                    s += "&\\cU{{{:d}}}".format(M[i][j])
        s += " \\\\\n"
    return s
