---
pagetitle: 'RSA and Diffie-Hellman'
author: 'Aurore Guillevic (Inria Nancy Grand-Est)'
date: 'November 12, 2020, updated February 14, 2024'
title: 'RSA, Diffie-Hellman and Pairings, Integer Factorization and Discrete Logarithm computation'
institute: 'Université de Bretagne Sud, Master CSSE'
---

-   [Preliminaries](#preliminaries)
    -   [Introduction: public-key cryptography](#introduction-public-key-cryptography)
    -   [Textbooks](#textbooks)
-   [RSA, and integer factorization problem](#rsa-and-integer-factorization-problem)
    -   [Introduction on RSA](#introduction-on-rsa)
        -   [RSA, how does it work?](#rsa-how-does-it-work)
        -   [Padding](#padding)
        -   [Malleability](#malleability)
        -   [Choosing key sizes](#choosing-key-sizes)
		-   [Examples: `https` connections](#examples-https-connections)
    -   [Attacking RSA with Integer Factorization](#attacking-rsa-with-integer-factorization)
        -   [Naive way 1: Testing all primes up to square root of *N*](#naive-way-1-testing-all-primes-up-to-square-root-of-n)
        -   [Naive way 2: testing all primes around square root of *N*](#naive-way-2-testing-all-primes-around-square-root-of-n)
        -   [Historical steps in integer factorization](#historical-steps-in-integer-factorization)
        -   [Quadratic sieve](#quadratic-sieve)
        -   [Nowadays' method: the Number Field Sieve](#nowadays-method-the-number-field-sieve)
        -   [Record computations](#record-computations)
    -   [Attacks on the RSA cryptosystem](#attacks-on-the-rsa-cryptosystem)
        -   [Too short keys: Humpich episode (1997 in France)](#too-short-keys-humpich-episode-1997-in-france)
        -   [Wrong key sizes: Bitcrypt ransomware (2014)](#wrong-key-sizes-bitcrypt-ransomware-2014)
        -   [Bad randomness: gcd, Coppersmith attacks](#bad-randomness-gcd-coppersmith-attacks)
-   [RSA and the quantum computer](#rsa-and-the-quantum-computer)
-   [Diffie-Hellman, and the discrete logarithm problem](#diffie-hellman-and-the-discrete-logarithm-problem)
    -   [Discrete logarithm problem and cryptosystems](#discrete-logarithm-problem-and-cryptosystems)
        -   [Discrete logarithm problem](#discrete-logarithm-problem)
        -   [Choice of group](#choice-of-group)
        -   [Diffie-Hellman key exchange](#diffie-hellman-key-exchange)
        -   [ElGamal, Schnorr signature, DSA](#elgamal-schnorr-signature-dsa)
    -   [Computing discrete logarithms](#computing-discrete-logarithms)
        -   [Generic algorithms of square root complexity](#generic-algorithms-of-square-root-complexity)
        -   [Index calculus algorithm](#index-calculus-algorithm)
        -   [Quadratic Sieve, Number Field Sieve](#quadratic-sieve-number-field-sieve)
        -   [Choosing key-sizes](#choosing-key-sizes-1)
    -   [Attacks on discrete-logarithm based cryptosystems](#attacks-on-discrete-logarithm-based-cryptosystems)
        -   [Sony Play-Station 3 (PS3) hacking](#sony-play-station-3-ps3-hacking)
        -   [Weak Diffie-Hellman and the logjam attack (2015)](#weak-diffie-hellman-and-the-logjam-attack-2015)
        -   [Weak keys in the Moscow internet voting system](#weak-keys-in-the-moscow-internet-voting-system)
    -   [Discrete logarithm computation in finite fields **F**<sub>2<sup>*n*</sup></sub> = GF(2<sup>*n*</sup>) and **F**<sub>3<sup>*m*</sup></sub> = GF(3<sup>*m*</sup>)](#discrete-logarithm-computation-in-finite-fields-f2n-gf2n-and-f3m-gf3m)
-   [Pairings](#pairings)
    -   [What is a pairing?](#what-is-a-pairing)
    -   [Pairings in cryptography: 1993 and 2001](#pairings-in-cryptography-1993-and-2001)
        -   [1993](#section)
        -   [2001](#section-1)
    -   [Pairings with curves over fields GF(2<sup>*n*</sup>) (and GF(3<sup>*m*</sup>)), rise and fall](#pairings-with-curves-over-fields-gf2n-and-gf3m-rise-and-fall)
    -   [Pairings with curves over fields **Z**/*p***Z** = **F**<sub>*p*</sub> = GF(*p*)](#pairings-with-curves-over-fields-zpz-fp-gfp)
    -   [Computing Discrete logarithms in **F**<sub>*p<sup>n</sup>*</sub> = GF(*p<sup>n</sup>*)](#computing-discrete-logarithms-in-fpn-gfpn)
        -   [Choosing key-sizes](#choosing-key-sizes-2)
-   [How to stay up-to-date (*veille technologique*)](#how-to-stay-up-to-date-veille-technologique)
    -   [ANSSI, NIST](#anssi-nist)
    -   [IACR events and publications](#iacr-events-and-publications)
    -   [Blogs](#blogs)
    -   [Mailing-lists](#mailing-lists)
    -   [Mathematical software](#mathematical-software)
    -   [Cours au Collège de France](#cours-au-collège-de-france)
    -   [Further reading](#further-reading)
        -   [PhD theses](#phd-theses)
        -   [Historical books](#historical-books)
-   [Index, English-French](#index-english-french)
    -   [Notations](#notations){#toc-notations}

These lecture notes are about integer factorization, discrete logarithm
computation, and pairings on elliptic curves, all for asymmetric cryptography.

# Preliminaries

## Introduction: public-key cryptography
Public-key cryptography, or asymmetric cryptography, is quite recent, it was
introduced in 1976 and 1977 with two cryptosystems, RSA
(Rivest-Shamir-Adleman in 1977) and DH (Diffie-Hellman in 1976).
In a public-key system, the keys are asymmetric, in the sense that the encryption
key and the decryption key are different, and it is a very hard mathematical
problem to find the decryption key while knowing only the public key (and the
public parameters).
It is an active research area to challenge believed hard mathematical problems
used in cryptography, and looking for new hard problems.
Nowadays two hard mathematical
problems are at the heart of public-key (or asymmetric) cryptography:
hardness of integer factorization with the RSA cryptosystem, and discrete logarithm
computation, related to the hardness of the Diffie--Hellman problem.

## Textbooks

Christof Paar and Jan Pelzl.
_Understanding Cryptography, a Textbook for Students and Practitioners_.
Springer, 2010.
DOI [10.1007/978-3-642-04101-3](https://doi.org/10.1007/978-3-642-04101-3)

(Two textbooks are available at the library at Lorient, code `005.8 PAA`).  
**During lockdown, the library is opened: see** [La BU sur
rendez-vous](https://www-actus.univ-ubs.fr/fr/index/actualites/scd/covid-19-la-bu-sur-rdv.html)  
**Pendant le confinement, la BU reste ouverte sur RDV : voir** [La BU sur rendez-vous](https://www-actus.univ-ubs.fr/fr/index/actualites/scd/covid-19-la-bu-sur-rdv.html)

Relevant chapters: 6, 7, 8 and 10.

-   Schnorr signature, patented until February 2008,
	[on Wikipedia](https://en.wikipedia.org/wiki/Schnorr_signature)
-   Digital Signature Standard DSS
    (previously known as Digital Signature Algorithm DSA),
    textbook (Paar Pelzl) Chapter 10 section 4 page 277,
    Handbook of Applied Cryptography [Chapter 11 section 5.1 (11.5.1)](http://cacr.uwaterloo.ca/hac/about/chap11.pdf),
    [on Wikipedia](https://fr.wikipedia.org/wiki/Digital_Signature_Algorithm),
    [NIST FIPS 186-5 Section 4](https://doi.org/10.6028/NIST.FIPS.186-5),
    released on February 2023, supercedes the previous
    [NIST FIPS 186-4 Section 4](https://www.nist.gov/publications/digital-signature-standard-dss-2?pub_id=914162)
-   ECDSA (elliptic curve DSA),
    textbook (Paar Pelzl) chapter 10 section 5 (10.5 page 282),
    [NIST FIPS 186-5 Section 6](https://doi.org/10.6028/NIST.FIPS.186-5),
    released on February 2023, supercedes the previous
    [NIST FIPS 186-4 Section 6](https://www.nist.gov/publications/digital-signature-standard-dss-2?pub_id=914162)
-   EdDSA (Edwards-Curve DSA),
    [NIST FIPS 186-5 Section 7](https://doi.org/10.6028/NIST.FIPS.186-5),
    released on February 2023.


On the webpage of [Pr. Alfred Menezes](http://www.math.uwaterloo.ca/~ajmeneze/)
one finds the chapters of the [_Handbook of applied cryptography_](http://cacr.uwaterloo.ca/hac/),
the relevant chapters are
[chapter 3](http://cacr.uwaterloo.ca/hac/about/chap3.pdf),
section 3.3 on the RSA problem,
section 3.6 on the discrete logarithm problem,
section 3.7 on the Diffie-Hellman problem;
[chapter 8](http://cacr.uwaterloo.ca/hac/about/chap8.pdf),
section 8.2 on public-key cryptography and RSA,
section 8.4 on ElGamal encryption,
and [Chapter 11](http://cacr.uwaterloo.ca/hac/about/chap11.pdf),
section 5.2 (11.5.2) on ElGamal signature scheme,
section 5.1 (11.5.1) on Digital Signature Algorithm.

In the following notes, the original research papers are given (name of authors,
title of paper, journal or conference where the results were published). In many
cases, only the abstract of the paper is available on the editor's website, the
PDF of the paper it accessible only after a paywall, or through identification
(you can try through eduroam, or try _institutional identification_, then try
(_Log in via Shibboleth or Athens_). Alternatively, a seach on
public archives can sometimes provide a free version:

-   [arxiv](https://arxiv.org/)
-   [HAL](https://hal.inria.fr/)
-   [ePrint](https://eprint.iacr.org/)
-   [iacr archive](https://www.iacr.org/archive/)
-   [Google scholar](https://scholar.google.fr/)

You don't need to read these papers for the course.

# RSA, and integer factorization problem

**Textbook _Understanding cryptography_, Chapters 6 and 7.**  
**_Handbook of applied cryptography_, section 3.3 of Chapter 3 and section 8.2 of
Chapter 8.**  
Wikipedia: [RSA cryptosystem](https://en.wikipedia.org/wiki/RSA_(cryptosystem)),
available in many languages (links on the left of the page).

In French:
[Nouveaux records de factorisation et de calcul de logarithme discret](https://inria.hal.science/hal-03045666)
Techniques de l'Ingénieur, December 2020.

## Introduction on RSA

### RSA, how does it work?
In 1977, Ron Rivest, Adi Shamir, and Leonard Adleman design an asymmetric
encryption scheme.
It requires a _modulus_ _N_, it is an integer, the result of a multiplication of
two distinct prime numbers _p_ and _q_. Arithmetic operations are performed
modulo _N_: addition, subtraction, multiplication and inversion. But other
operations are hard (take an extremely long time) if nothing more than _N_ is
known: computing a square root: sqrt(_x_) = _x_<sup>1/2</sup> modulo _N_ is
hard, computing _x_<sup>1/_n_</sup> modulo _N_ is hard, because the order of the
_multiplicative group_ is not known.

The _multiplicative group_ is the set of elements _x_ in {1, 2, 3, …, _N_-1} that
have an inverse 1/_x_ modulo _N_. To be invertible, _x_ needs to be co-prime to
_N_, that is, _x_ and _N_ have no common factor. But since _N_ = _p_ · _q_ and _p_, _q_
are prime, one can list the numbers that are _not_ coprime to _N_: they are
{*p*, 2*p*, 3*p*, …, (_q_-1)_p_} (set of _q_-1 numbers) and
{_q_, 2*q*, 3*q*, …, (_p_-1)_q_} (set of _p_-1 numbers). Removing (_q_-1+_p_-1) integers
from the set {1, 2, …, _N_-1}, there are _N_-1-(_q_+_p_-2) = _pq_ -_p_-_q_+1 =
(_p_-1)(_q_-1) integers in the multiplicative group.
But computing the value (_p_-1)(_q_-1) without knowing _p_ and _q_ is very hard
when _N_ is large enough (more than 600 decimal digits for example).

The encryption scheme of Rivest, Shamir and Adleman works as follows, between
two parties usually named Alice and Bob.

-   Alice chooses two distinct primes _p_ and
    _q_ and computes _N_= _pq_. She chooses a positive integer _e_ coprime
    to _p_ and _q_ (in reference standards, _e_ = 2<sup>16</sup> + 1 = 65537) and she computes _d_ the
    inverse of _e_ modulo (_p_ - 1)(_q_ - 1), so that _ed_ = 1 mod (_p_ - 1)(_q_ - 1). Her
    public key is the pair (_N_, _e_) and her secret key is made of the three
    numbers (_p_, _q_, _d_);
-   Bob reads Alice's public key (_N_, _e_); he encodes his secret as a number _m_
    between 1 and _N_ - 1. To encrypt _m_, he computes _c_ = _m<sup>e</sup>_ mod _N_ and sends
    _c_ to Alice;
-   Alice receives _c_ and computes _c<sup>d</sup>_ mod _N_. She has
    _c<sup>d</sup>_ = _m<sup>ed</sup>_ = _m_ mod _N_.

The public exponent _e_ is for **encryption**, and the private exponent _d_ is for **decryption**.
It works because with _ed_ = 1 mod (_p_ - 1)(_q_ - 1), one has _m<sup>ed</sup>_ = _m_ mod _N_.
The security of this scheme relies on the hardness of computing the secret
exponent _d_, knowing only _N_ and the public exponent _e_. The secret number _p_ and _q_ are
required to compute _d_, hence the security of RSA relies on the hardness of
factoring _N_.

In practice, one can generate a pair of asymmetric keys with `ssh-keygen`
in a Linux shell, with PGP for emails (Enigmails on Thunderbird, Protonmail for
example).

Note that short keys are not allowed:
```shell
ssh-keygen -b 512 -t rsa -f key_example_512
Invalid RSA key length: minimum is 1024 bits
```

```shell
ssh-keygen -b 1024 -t rsa -f key_example
```
The files `key_example` (with the private key) and `key_example.pub` (public
key, public exponent) are encoded in Base64.

### Knowing the public and private exponents gives a factorization of _N_
Knowing _d_ and _e_ satisfying _ed_ = 1 mod (_p_-1)(_q_-1),
one can factor _N_ and recover _p_ and _q_.  
For all _x_ in {1, …, _N_-1} coprime to _N_, we have _x_<sup>_ed_-1</sup> = 1 mod _N_
because _ed_-1 = 0 mod (_p_-1)(_q_-1), and _x_<sup>(_p_-1)(_q_-1)</sup> = 1 mod _N_.
Since _p_ and _q_ are prime, (_p_-1)(_q_-1) is even (it is a multiple of 4) and
since _ed_-1 is a multiple of (_p_-1)(_q_-1), it is a multiple of 4. We can
divide _ed_-1 by two: (_ed_-1)/2 is an integer.

We consider an _x_ in {1, …, _N_-1} coprime to _N_.
We compute _y_ = _x_<sup>(_ed_-1)/2</sup>. It is a square root of 1. If the
result _y_ is not 1 or -1, then we have _y_<sup>2</sup> = 1<sup>2</sup> mod _N_,
that is _y_<sup>2</sup> - 1<sup>2</sup> = (_y_-1)(_y_+1) = 0 mod _N_. Hence _y_-1
and _y_+1 share a factor with _N_.
We compute gcd(_y_-1, _N_) to find a factor _p_ or _q_.
It might be 1 or _N_, in that case we restart with another _x_.

If the value of _y_ is 1 or -1, then we can do the same but with _y_ =
_x_<sup>(_ed_-1)/4</sup>,  _y_ = _x_<sup>(_ed_-1)/8</sup>, etc, and keep dividing the
exponent by increasing powers of 2 as long as the result is an integer.
If it fails, we can try with another _x_.
In practice, the chances of success are very high (only a few _x_ are needed).


### Short private exponent is a bad idea

Wiener found an [attack](https://www.cits.ruhr-uni-bochum.de/imperia/md/content/may/krypto2ss08/shortsecretexponents.pdf)
when the private exponent _d_ is very small compared to _N_.


### Padding
With a modulus _N_, one can encrypt messages _m_ between 0 and _N_ - 1. But if _m_
is 0, 1 or much smaller than _N_, it produces a bias, and problems can happen.
For example, the ciphertext of 0 is 0 (if _m_ = 0, _m_<sup>e</sup> = 0 mod _N_),
the ciphertext of 1 is 1 (if _m_ = 1, _m_<sup>e</sup> = 1 mod _N_),
and if _m_ is between 2 and _N_<sup>1/e</sup> (the nearest integer to the _e_-th
root of _N_), there is no modular reduction, _c_ = _m_<sup>_e_</sup> as
integers.
RSA encryption scheme cannot be used directly as described above. It should be
used with a good **padding**, that is a way to fill the unused bytes of data when
_m_ is shorter (in bytes) than _N_.

In particular, if a symmetric key of 128 bits (16 bytes) is encrypted with a
2048-bit modulus _N_ (256 bytes), then 2048 - 128 = 1920 bits (240 bytes) are free.
Standards like PKCS describe secure ways of implementing a padding (filling the
zeros).

### Malleability
Given two messages _m_<sub>1</sub> and _m_<sub>2</sub>,
their respective ciphertexts are

-   _c_<sub>1</sub> = _m_<sub>1</sub><sup>e</sup> mod _N_ and
-   _c_<sub>2</sub> = _m_<sub>2</sub><sup>e</sup> mod _N_

The product of the ciphertexts is the ciphertext of the product of the
plaintexts:

>   _c_<sub>1</sub> _c_<sub>2</sub> mod _N_ = (_m_<sub>1</sub> _m_<sub>2</sub>)<sup>e</sup> mod _N_

This property is called **malleability**.
**Padding** should be used to avoid it.

### Choosing key sizes
Contrary to symmetric cryptography where the key sizes are usually 128, 192 or
256 bits, with RSA the key sizes are **much larger**.
Cryptographers are interested in comparing the security of asymmetric cryptosystems to
the security of symmetric ciphers (like AES). For a perfect symmetric cipher, whose secret
key has length _n_ bits, the generic attack consists in testing all the 2<sup>_n_</sup>
possible keys. This is also called brute-force search. Hence a symmetric cipher
of keylength _n_ bits is said to provide _n_ bits of security.
The length of RSA
modulus _N_ is designed in order to ensure that an attacker attempting to factor
_N_ would need as much time as for breaking a perfect symmetric encryption with
a key of 128, 192, or 256 bits. Because the hardness of factoring grows slower,
the key sizes are much larger.

In a web browser, one can check the _cipher suite_ used for securely connecting
to a `https` website. A cipher suite is made of a public-key and a private-key
encryption system.

### Examples: `https` connections
Connecting to [lemonde.fr](https://www.lemonde.fr/), the connection is encrypted
with `TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, 128 bits, TLS 1.2`.

[Qualys SSL Labs - SSL Pulse](https://www.ssllabs.com/ssl-pulse/) monitors TLS
connections and monthly scans websites. Below is taken from December 2023 report.

![Key Exchange Strength](img/ssllabs-keyexchange-sizes.png)
![Key Strength Distribution](img/ssllabs-keystrength-sizes.png)

![Graph of complexity of integer factorization, discrete logarithm with NFS, and
generic DL computation](img/extrapolation-asymptotic-complexities-1.png)

#### Particles

_n_ | 2<sup>_n_</sup> | Examples
:----:|:----------:|:------------------------------------------------
 32 | 2<sup>32</sup> = 10<sup>9.6</sup> | number of humans on Earth
 47 | 2<sup>47</sup> = 10<sup>14.2</sup> | distance Earth - Sun in millimeters (149.6×10<sup>12</sup>), number of operations in one day on a processor at 2 GHz
 56 | 2<sup>55.8</sup> = 10<sup>16.8</sup> | number of operations in one year on a processor at 2 GHz
 79 | 2<sup>79</sup> = 10<sup>23.8</sup> | Avogadro number: atoms of Carbon 12 in 1 mol
 82 | 2<sup>82.3</sup> = 10<sup>24.8</sup> | mass of Earth in kilogrammes
100 | 2<sup>100</sup> = 10<sup>30</sup> | numer of operations in 13.77 billion years (age of the universe) on a processor at 2 GHz
 155 | 2<sup>155</sup> = 10<sup>46.7</sup> | number of molecules of water on Earth
 256 | 2<sup>256</sup> = 10<sup>77.1</sup> | number of electrons in universe
    
(Courtesy Marine Minier)


#### Boiling water

Lenstra, A.K., Kleinjung, T., Thomé, E.: Universal Security; From bits and
    mips to pools, lakes - and beyond. In: Fischlin, M., Katzenbeisser, S.
    (eds.) Number Theory and Cryptography. LNCS,
    vol. 8260, pp. 121-124. Springer, Darmstadt, Germany (Nov 2013).
    DOI [10.1007/978-3-642-42001-6_9](http://doi.org/10.1007/978-3-642-42001-6_9),
	HAL [00925622](https://hal.inria.fr/hal-00925622).

-   2<sup>90</sup> operations require enough energy to boil the lake of Genève
-   2<sup>114</sup> operations: boiling all the water on Earth
-   2<sup>128</sup> operations: boiling 16000 planets like the Earth


## Attacking RSA with Integer Factorization
One way of breaking RSA encryption is to factor the modulus _N_ into _p_ times _q_.
Then, computing _d_ = 1/_e_ mod (_p_ - 1)(_q_ - 1) is easily done with an
**extended gcd** algorithm (with SageMath for example).

### Naive way 1: Testing all primes up to square root of _N_
For very small numbers (up to 1000 for example), factoring it can be done by
trying to divide it by all the prime numbers up to its square root.
In math software this is called **trial division**. For large numbers this is
impracticable: there are too many possible prime factors.
There are about _N_/ln _N_ prime numbers up to _N_, where ln is the logarithm in
basis e (not in basis 10). Restricting the search to
the prime numbers up to the square root of _N_, it still has a cost of roughly
the square root of _N_ over log _N_ tests.

| _N_ (bits)   | _N_ (digits) | sqrt(_N_)/ln(sqrt(_N_)) |
|:------------:|:------------:|:-----------------------------:|
|   256  |    77  |  2<sup>122</sup>,     10<sup>37</sup>   |
|   512  |   154  |  2<sup>249</sup>,     10<sup>75</sup>   |
|   768  |   231  |  2<sup>376</sup>,     10<sup>114</sup>  |
|  1024  |   308  |  2<sup>504</sup>,     10<sup>152</sup>  |
|  1280  |   385  |  2<sup>632</sup>,     10<sup>191</sup>  |
|  1536  |   462  |  2<sup>759</sup>,     10<sup>229</sup>  |
|  1792  |   539  |  2<sup>887</sup>,     10<sup>267</sup>  |
|  2048  |   617  |  2<sup>1015</sup>,    10<sup>306</sup>  |
|  2304  |   694  |  2<sup>1143</sup>,    10<sup>344</sup>  |
|  2560  |   771  |  2<sup>1271</sup>,    10<sup>383</sup>  |
|  2816  |   848  |  2<sup>1399</sup>,    10<sup>421</sup>  |
|  3072  |   925  |  2<sup>1526</sup>,    10<sup>460</sup>  |

```python
from math import log as ln
from math import ceil
for log2N in range(256, 3072+1, 256):
    log10N = int(round(log2N*ln(2)/ln(10)))
    s2 = int(ceil(log2N/2 - ln(log2N/2*ln(2))/(ln(2))))
    s10 = int(ceil(log2N/2 * ln(2)/ln(10) - ln(log2N/2 * ln(2))/ln(10)))
    print("|  {:4d}  |  {:4d}  |  2^{:d}  |  10^{:d}  |".format(log2N, log10N, s2, s10))
```

> This technique is completely impracticable even for numbers of 30 digits,
> because there are too many prime numbers that need to be tested.


### Naive way 2: testing all primes around square root of _N_
Actually we know that _p_ and _q_ are both of the same size.
How many primes are they between 2<sup>_n_-1</sup> and 2<sup>_n_</sup> where
_n_ is the expected size of _p_ and _q_?

count = 2<sup>_n_</sup>/ln(2<sup>_n_</sup>)
  - 2<sup>_n_-1</sup>/ln(2<sup>_n_-1</sup>)
  = 2<sup>_n_-1</sup>(2/(_n_ ln 2) - 1/((_n_ - 1) ln 2))
  = (2<sup>_n_-1</sup>/ln 2) · (2/_n_ - 1/(_n_ - 1))
  approximately (1/2) · (2<sup>_n_</sup> / ln 2<sup>_n_</sup>)

```python
from math import log as ln
from math import ceil
for log2N in range(256, 3072+1, 256):
    log10N = int(round(log2N*ln(2)/ln(10)))
    n = log2N / 2
    c = (n-1)*ln(2) + ln((n-2)/(ln(2)*n*(n-1)))
    c2 = int(ceil(c/ln(2)))
    c10 = int(ceil(c/ln(10)))
    print("|  {:4d}  |  {:4d}  |  2^{:d}  |  10^{:d}  |".format(log2N, log10N, c2, c10))
```

| _N_ (bits)   | _N_ (digits) | count: prime numbers around sqrt(_N_) |
|:------------:|:------------:|:-----------------------------:|
|   256  |    77  |  2<sup>121</sup>,     10<sup>37</sup>   |
|   512  |   154  |  2<sup>248</sup>,     10<sup>75</sup>   |
|   768  |   231  |  2<sup>375</sup>,     10<sup>113</sup>  |
|  1024  |   308  |  2<sup>503</sup>,     10<sup>152</sup>  |
|  1280  |   385  |  2<sup>631</sup>,     10<sup>190</sup>  |
|  1536  |   462  |  2<sup>758</sup>,     10<sup>229</sup>  |
|  1792  |   539  |  2<sup>886</sup>,     10<sup>267</sup>  |
|  2048  |   617  |  2<sup>1014</sup>,    10<sup>306</sup>  |
|  2304  |   694  |  2<sup>1142</sup>,    10<sup>344</sup>  |
|  2560  |   771  |  2<sup>1270</sup>,    10<sup>383</sup>  |
|  2816  |   848  |  2<sup>1398</sup>,    10<sup>421</sup>  |
|  3072  |   925  |  2<sup>1525</sup>,    10<sup>460</sup>  |

> There are again many prime numbers of a fixed size in bits.
> It is only two times faster than the previous method (for example the first
> rows of both tables say 2<sup>122</sup> = 2 × 2<sup>121</sup>). It is not feasible.

### Historical steps in integer factorization

-   1975, Morrison, Brillhard, continued fraction method CFRAC, factorization
    of the 7-th Fermat number 2<sup>2<sup>7</sup></sup>+1 = 2<sup>128</sup>+1
	(see the [Cunningham project](https://homes.cerias.purdue.edu/~ssw/cun/))  
	2<sup>128</sup>+1 = 340282366920938463463374607431768211457 =
	59649589127497217 × 5704689200685129054721
-   1981, Dixon, random squares method
-   70's, unpublished: Schroeppel, Linear Sieve
-   1982, Pomerance, Quadratic Sieve
-   1987, Lenstra, Elliptic Curve Method (ECM)
-   1993, Buhler, Lenstra, Pomerance, General Number Field Sieve

A strong joint work between academia and industry in particular in the US:
showing that very large computations can be run on computers was a selling
argument for industrial too (it was _before_ the _personal computer_).

### Quadratic sieve
Find a very different approach: we know that _N_ is the product of two prime
numbers.

#### Square roots modulo _N_
In a field like the real numbers **R**, a positive number _n_ has two square
roots: +sqrt(_n_) and -sqrt(_n_). But in a **ring** of integers modulo _N_
=_pq_, strange things happen: if a number is a square, it has four square roots.

> Example: _N_ = 2021 = 43 × 47  
>
>>   1<sup>2</sup> = 1 mod 2021  
>>   (-1)<sup>2</sup> = 1 mod 2021  
>>   988<sup>2</sup> = 1 mod 2021  
>>   (-988)<sup>2</sup> = 1 mod 2021  

```python
N = 2021
for i in range(-N//2, N//2):
    if (i**2 % N) == 1:
        print(i)
```
We have two pairs of square roots of _n_=1: (1, -1) and (988, -988).
Then, observe that

988<sup>2</sup> = 1<sup>2</sup> mod 2021  
⇔ 988<sup>2</sup> - 1<sup>2</sup> = 0 mod 2021  
⇔ (988 - 1) · (988 + 1) = 0 mod 2021  

Compute a gcd: gcd(988 - 1, _N_) = 47, hence 47 divides _N_. Indeed _N_ = 43×47.
(It works with gcd(988 + 1, _N_) = 43 too.)

> **_B_-smooth numbers**  
> A positive integer _n_ is _B_-smooth if and only if
> _n_ factors as a product of primes (or prime powers) up to the smoothness bound _B_:  
> _n_ = 2<sup>_e_<sub>1</sub></sup> 3<sup>_e_<sub>2</sub></sup>5<sup>_e_<sub>3</sub></sup>…_p<sub>i</sub><sup>e<sub>i</sub></sup>_
> and _p<sub>i</sub>_ < _B_.
_B_-smooth integers are quite common.

> **Aim of the quadratic sieve method**:  
> finding a relation _X_<sup>2</sup> = > _Y_<sup>2</sup> mod _N_,
> where _X_ is different from _Y_ and -_Y_,
> to obtain a factorization (_X_ - _Y_) · (_X_ + _Y_) which is a multiple of _N_.
> Then finding the factorization of _N_ by computing a gcd of _N_ with _X_ - _Y_ or _X_ + _Y_.

But of course, we cannot enumerate the squares of numbers modulo _N_ to find a
match, and none of the methods for computing a square root in a field will work.

**Brilliant idea**: enumerate smoothness relations, then combine them to obtain a
combination of squares.

#### Numerical example
Again one starts with _N_ = 2021.
One would like to find an equality modulo _N_ with a distinct square on both
sides.
Start with a square on the left-hand side, and factor the right-hand side (as an
integer) into a product of small primes.
Hope to be able to combine the relations to obtain a square on the right too.

One starts with enumerating the integers right above sqrt(_N_). Their square is
larger than _N_, hence the operation _modulo N_ subtracts _N_.
In other words, set _m_ = round(sqrt(_N_)) the nearest integer to the square
root of _N_, and lists all small integers _a_ such that

> the integer (_m_+_a_)<sup>2</sup> - _N_ is the product of small primes only.

_m_ = 45 (where sqrt(2021) = 44.955…)  
(1+_m_)<sup>2</sup> - _N_ = 95 = 5 × 19  
(4+_m_)<sup>2</sup> - _N_ = 380 = 2<sup>2</sup> × 5 × 19  
(16+_m_)<sup>2</sup> - _N_ = 1700 = 2<sup>2</sup> × 5<sup>2</sup> × 17  

→ We enumerated the _a_ from 1 to 20.

The left-hand side of the relations is a square by design, let us concentrate on the
right-hand side.
One combines the first and second relation: multiplying the two left-sides, and
separately the two right-sides: one obtain that

> (1+_m_)<sup>2</sup> · (4+_m_)<sup>2</sup> = 2<sup>2</sup> · 5<sup>2</sup> · 19<sup>2</sup> mod _N_
> ⟹ (46 · 49)<sup>2</sup> = (2 · 5 · 19)<sup>2</sup> mod _N_

Set _X_ = 46 · 49 = 2254 = 233 mod _N_, _Y_ = 2 · 5 · 19 = 190, and finally
gcd(233 - 190, _N_) = 43, we found a factor of _N_.
It works with gcd(233 + 190, _N_) = 47, this is the other factor of _N_.

This method keeps only the relations modulo _N_ with _small primes_ for two reasons:

1.  easier to factor a number if we know that is has only small factors. If no
    small factor is found, the factorization process is stopped before the end.
2.  Focusing on small factors will improve the chances to be able to find a
    combination so that all the factors are square numbers.

The techniques of factorization into only small prime numbers evolved to
_smoothness detection tests_. (Smoothness is _friabilité_ in French, it means an
integer which is the product of prime numbers smaller than a given bound. The
prime numbers can have some power: 32 is 2-smooth because 32 = 2<sup>5</sup>).

The step of finding a combination of the relations to obtain a square on the
right is done with linear algebra: store the exponents in a matrix, actually
store only if the exponent is odd or even (1 or 0 modulo 2), and compute the
_left kernel_ of the matrix. Linear algebra on very huge matrices is a research
field in itself.

#### Limitations of the quadratic sieve

Complexity: exp(sqrt((2 + o(1)) ln _N_ ln ln _N_))

-  The integers to be factored into small primes are
   _n_ = (_a_+_m_)<sup>2</sup> - _N_ of size about
   _A_ sqrt(_N_) where _A_ is the bound on the small integers _a_. For _N_ larger
   than 100 decimal digits, it is not competitive.
-  the smoothness bound determines the size of the linear system to solve:
   as we have seen, there are _B_/ln(_B_) prime numbers up to a bound _B_,
   hence keeping the relations where the right-hand side is _B_-smooth results
   in a linear system with a quite huge matrix, of _B_/ln(_B_) rows and columns.
   If _B_ is not large enough, there will not be enough relations and no
   solution will be found.
-  The aim is to find the best trade-off between _A_ and _B_ to minimize the cost of the
   algorithm.

![Graph of complexity of integer factorization and discrete logarithm with NFS,
quadratic sieve, naive integer factorization, and generic DL
computation](img/extrapolation-asymptotic-complexities-quadratic-sieve-1.png)

### Nowadays' method: the Number Field Sieve
(in French: Crible Algébrique)

The Number Field Sieve was developed in the 80's and 90's for integer
factorization and discrete logarithm computation.
Observing that the bottleneck of the quadratic sieve is the million of
factorizations of numbers of the size of sqrt(_N_), the aim is to reduce the
size of these numbers.

The mathematics involved in the Number Field Sieve are beyond the scope of this
document.
In short, there are two main steps: collecting many relations between smooth numbers, and
then solving a very large _sparse_ linear system.

The numbers to be factored are much smaller compared to the quadratic sieve.
There are two small integers _a_ and _b_ (instead of one _a_ in the quadratic sieve).
The integers to factor are of size _A_<sup>d</sup> _N_<sup>1/_d_</sup> where _d_
is some degree between 3 and 6 in practical implementations, and _A_ is a bound
on _a_ and _b_.

The linear system to solve is _sparse_. A _sparse_ matrix has most of
its entries set to zero. The average number of non-zero coefficients per row
is called the _density_. In the case of a sparse matrix, computing a kernel
costs about O(_n_<sup>2</sup>) where _n_ is the number of rows and columns (for
a square matrix).

### Record computations

The latest record computation of factoring an RSA challenge was performed by the
researchers Boudot, Gaudry, Guillevic, Heninger, Thomé and Zimmermann from
France and US, with computing power in Europe and US.
See [_Nouveaux records de factorisation et de calcul de logarithme discret_](https://inria.hal.science/hal-03045666)
for a presentation of the record computations **in French**.

Here is a figure to compare the record computations of Integer Factorization and
Discrete Logarithm computation, and a table of the RSA number factorizations
with the Number Field Sieve.

![Record computations: RSA modulus factorization and discrete logarithm
    computation in prime fields.](img/records-IF-DL-1.png)

| _N_ | decimal digits | bits | Day | Authors |
|:-----:|:-----:|:---:|------------|----------------------------------------------------------|
 RSA-100   | 100 | 330 | April     1, 1991 | Lenstra
 RSA-110   | 110 | 364 | April    14, 1992 | Lenstra, Manasse
 RSA-120   | 120 | 397 | July      9, 1993 | Denny, Dodson, Lenstra, Manasse
 RSA-129   | 129 | 426 | April    26, 1994 | Atkins, Graff, Lenstra, Leyland
 RSA-130   | 130 | 430 | April    10, 1996 | Cowie, Dodson, Elkenbracht-Huizing, Lenstra, Montgomery, Zayer
 RSA-140   | 140 | 463 | February  2, 1999 |  Cavallar, Dodson, Lenstra, Leyland, Lioen, Montgomery, Murphy, te Riele, Zimmermann
 RSA-_512_ | 155 | 512 | August   22, 1999 | Cavallar, Dodson, Lenstra, Lioen, Montgomery, Murphy, te Riele, Aardal, Gilchrist, Guillerm, Leyland, Marchand, Morain, Muffett, Putnam, Putnam, Zimmermann
 RSA-160   | 160 | 530 | April     1, 2003 | Bahr, Franke, Kleinjung, Lochter, Böhm
 RSA-_576_ | 174 | 576 | December  3, 2003 | Franke, Kleinjung, Montgomery, te Riele, Bahr
 RSA-200   | 200 | 663 | May       9, 2005 | Bahr,  Böhm, Franke, Kleinjung
 RSA-_768_ | 232 | 768 | December 12, 2009 | Kleinjung, Aoki, Franke, Lenstra, Thomé, Bos, Gaudry, Kruppa, Montgomery, Osvik, te Riele, Timofeev, Zimmermann
 RSA-240   | 240 | 795 | December  2, 2019 | Boudot, Gaudry, Guillevic, Heninger, Thomé, Zimmermann
 RSA-250   | 250 | 829 | February 28, 2020 | Boudot, Gaudry, Guillevic, Heninger, Thomé, Zimmermann

Integer factorization records since the RSA challenge, from
_Nouveaux records de factorisation et de calcul de logarithme discret_,
Boudot, Gaudry, Guillevic, Heninger, Thomé, Zimmermann, Techniques de
l'ingénieur, 2020. [Version auteur ici](https://inria.hal.science/hal-03045666)
See also
[RSA Factoring Challenge on Wikipedia](https://en.wikipedia.org/wiki/RSA_Factoring_Challenge).

## Attacks on the RSA cryptosystem

A survey paper by Dan Boneh in 1999:

-  [Twenty years of attacks on the RSA cryptosystem](http://www.ams.org/notices/199902/boneh.pdf)

### Too short keys: Humpich episode (1997 in France)
[The story, in French](http://www.bibmath.net/crypto/index.php?action=affiche&quoi=moderne/cb)
(also, Le Monde 24.01.2000, page 8, 11.03.2000, 24.01.2001)
After the affair, on March 4, 2000 the 96 decimal-digit key of GIE carte
bancaire (groupement d'intérêt économique) was released on the internet.
It was then possible for anyone with adequate hardware to forge _yescards_.
([example of newspapers](https://www.liberation.fr/evenement/2000/03/10/des-hackers-revelent-sur-le-net-le-code-chiffre-des-cartes-bancaires-la-carte-a-puce-a-perdu-son-sec_318762))

In 1997, the RSA keys were 320-bit long. It was already extremely short: the
academic record factorization in 1996 was 130 decimal digits, which is 430 bits.
320 bits means 96 decimal digits, it is possible to factor such a number with
the quadratic sieve.

Nowadays, the keys are still quite short: 1152 bits in 2020.

### Wrong key sizes: Bitcrypt ransomware (2014)

[Web Page](https://airbus-cyber-security.com/fr/bitcrypt-broken/)
(Airbus Cybersecurity, formerly Cassidian).
In February 2014, a new ransomware named BitCrypt started to ransom random users.
Two computer scientists in France, Fabien Perigaud and Cédric Pernet,
investigated the first version of this malware. It searches for files on the
disk such as pictures, generates an AES (symmetric) key en encrypts the picture
with it. Then each symmetric key is encrypted with a unique RSA key. The malware
claims a 1024-bit RSA encryption, but actually the RSA key was
31298847196625400639506938637161930162789011464295952600544145829335849533528834917800088971765784757175491347320005860302574523
of 128 decimal digits. This was a unit mistake: 1024 bits are 128 bytes (one
byte is 8 bits), but 128 digits correspond to 426 bits.

With the free software cado-nfs, the two cryptographers Perigaud and Pernet were
able to factor the RSA modulus: the two prime factors are
_p_ = 4627583475399516037897017387039865329961620697520288948716924853
and
_q_ = 6763540271723193027434512605129229364869394444394656022641769391.
Then they obtained the secret RSA key to decipher the AES
symmetric keys, and finally recovered the encrypted data. But one week later, the
malware was updated with a new 1024-bit RSA key, which is out of reach for
factorization.

### Bad randomness: gcd, Coppersmith attacks

#### Gcd attack (2012, 2013)

We saw above that there are many distinct prime numbers of a given size: for a
2048-bit RSA modulus _N_ (617 digits), there are about 2<sup>1014</sup>, that is
10<sup>306</sup> distinct prime numbers of 1024 bits (308-309 decimal digits).
In other words, the set where to choose _p_ and _q_ is extremely large.
It is large enough to generate billions of distinct RSA keys, as long as the
randomness is of good quality: one need to be sure that no one will generate the
same _p_ independently.

In 2012 and 2013, different teams of researchers were able to obtain the private
keys _p_ and _q_ of some rare public keys _N_ by scanning the certificates on
the internet, then computing massively gcd (greatest common divisor) over all
the keys. They obtained gcd values larger than one, this is a factor of two
distinct public keys! More precisely, they found that in some cases, two
distinct keys _N_<sub>1</sub> and _N_<sub>2</sub> share one common factor _p_:
_N_<sub>1</sub> = _pq_<sub>1</sub> and _N_<sub>2</sub> = _pq_<sub>2</sub>.
In such a case, the two public keys _N_<sub>1</sub> and _N_<sub>2</sub> can be
easily factored.

Over a set of _n_ RSA public keys of the same size, a naive computation of gcds
of all pairs of keys requires _n_(_n_-1)/2 tests.
To improve the runnung-time of this massive gcd computation, the authors used a
technique called _batch gcd_. One multiplies together batches of RSA public keys
before computing gcds.

-  Nadia Heninger, Zakir Durumeric, Eric Wustrow, and J. Alex Halderman.
   **Mining your Ps and Qs: Detection of widespread weak keys in network
   devices.**
   In Tadayoshi Kohno, editor, _USENIX Security 2012_, pages
   205-220. USENIX Association, August 2012.
   [Video and paper](https://www.usenix.org/conference/usenixsecurity12/technical-sessions/presentation/heninger)
-  Arjen K. Lenstra, James P. Hughes, Maxime Augier, Joppe W. Bos, Thorsten
   Kleinjung, and Christophe Wachter.
   **Public keys.**
   In Reihaneh Safavi-Naini and Ran Canetti, editors, _CRYPTO 2012_,
   volume 7417 of _LNCS_, pages 626-642. Springer, Heidelberg, August 2012.
   DOI [10.1007/978-3-642-32009-5_37](https://doi.org/10.1007/978-3-642-32009-5_37)
   [free IACR archive](https://www.iacr.org/archive/crypto2012/74170620/74170620.pdf)

#### Coppersmith attack (2013)

In cases where the randomness is bad or wrongly used, a _Coppersmith attack_ can happen.
It was the case in 2013 over a large set of ID cards in the Taiwan system of
digital ID.

##### First steps: gcd attack and further pattern testings

-   More than 2 million of 1024-bit RSA public keys (2 086 177)
-   Batch gcd over the keys: 103 public keys factor into 119 different primes
    (one needs 206 distinct primes to generate 103 totally distinct RSA keys).
-   Pattern found in the primes: there was no underlying proper use of a good
    random-number generator.
-   Testing all primes following the expected pattern (164 primes) produced 18
    more factorizations. Further investigation gave 4 more factorizations.

The most common prime factor (found in 46 distinct RSA modules) was
_p_ = 2<sup>511</sup> + 2<sup>510</sup> + 761
wich is the next prime after 2<sup>511</sup> + 2<sup>510</sup>.

##### Next step: Coppersmith attack

Because of the function `next_prime` used in the (wrong) prime generation
process, the least significant bits do not follow a pattern. However, when the
most significants bits of a number _N_ are known, Coppersmith's technique allows
to recover a factor of _N_.

The following SageMath code reproduces the example from the paper:

```python
p = next_prime(2**511 + 2**510)
q = 0xc92424922492924992494924492424922492924992494924492424922492924992494924492424922492924992494924492424922492924992494924492424e5
N = p * q

X = 2**168
a = 0xc9242492249292499249492449242492249292499249492449242492249292499249492449242492249292499249492449242492249292499249492449242492

M = Matrix(3, 3, [X**2, X*a, 0, 0, X, a, 0, 0, N])
R = M.LLL()
g0 = R[0][2]
g1 = R[0][1] // X
g2 = R[0][0] // X**2
c = gcd([g0,g1,g2]) # gcd of coefficients
ZZx.<x> = ZZ[]
g = (g0 + g1*x + g2*x**2) // c
g.factor()
# (x - 83) * (30064312327*x - 23972510637500)
g(83) == 0
q == a + 83
```

it works even if one of the primes is a good random-looking prime:

```python
pi = 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651328230664709384460955058223172535940812848111745028410270193852110555964462294895493038196442881097566593344612847564823378678316527120190914564856692346034861

p = next_prime(floor(3*10**153 * pi))
# pattern
a = 0xabcdef1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef1234567890
mask = 0x373487da48205b420e32198eb592f66ff4036668142 (a random-looking integer of 170 bits)
q = next_prime(a + mask)
N = p*q

X = 2**170
M = Matrix(3, 3, [X**2, X*a, 0, 0, X, a, 0, 0, N])
R = M.LLL()
g0 = R[0][2]
g1 = R[0][1] // X
g2 = R[0][0] // X**2
c = gcd([g0,g1,g2]) # gcd of coefficients
ZZx.<x> = ZZ[]
g = (g0 + g1*x + g2*x**2) // c
g.factor()
# (x - 1290919795022495591476339394838400757312676461707819) * (285540389921735875044941860691872438862439969180237*x - 2764078888444839645345836360217456914343392132618412376463107078920699665583816722965783600691378228578)
r = g.roots()[0][0]
q == a + r # True
```

-  Daniel J. Bernstein, Yun-An Chang, Chen-Mou Cheng, Li-Ping Chou, Nadia
   Heninger, Tanja Lange, and Nicko van Someren.
   **Factoring RSA keys from certified smart cards: Coppersmith in the
   wild.**
   In Kazue Sako and Palash Sarkar, editors, _ASIACRYPT 2013, Part II_, volume
   8270 of _LNCS_, pages 341-360. Springer, Heidelberg, December 2013.
   DOI
   [10.1007/978-3-642-42045-0_18](https://doi.org/10.1007/978-3-642-42045-0_18)
   [free IACR archive](http://www.iacr.org/archive/asiacrypt2013/82700341/82700341.pdf)

# RSA and the quantum computer

If one day a powerful enough quantum computer exists, it will be able to break
any RSA public key by factoring the modulus, but such a computer does not seem
to come in the next ten years (we are in 2020).
As said in
[_Nouveaux records de factorisation et de calcul de logarithme discret_](https://inria.hal.science/hal-03045666):

> In 1994, Peter Shor designed an algorithm for integer factorization with a
> quantum computer, which is much faster. The factorization of a _n_-bit
> integer requires a perfect quantum computer with 2*n* qbits (quantum bits).
> Such a computer seems extremely hard to build. The record computation is
> actually very small: in 2018, it was 4 088 459 = 2017 × 2027. And it is not
> clear that the factorization process can be adapted to any other integer of
> seven digits. To put a long story short, most probably RSA-1024 (in bits) will
> be factored before a quantum compter become competitive.

# Diffie-Hellman, and the discrete logarithm problem

In the 70's in the US, another cryptosystem was invented: the Diffie-Hellman key
exchange (Merkle was also involved in the patent). This scheme is based on a
different hard mathematical problem: the _discrete logarithm problem_.

**Textbook _Understanding cryptography_, Chapters 6 and 7.**  
**[_Handbook of applied cryptography_](http://cacr.uwaterloo.ca/hac/),
sections 3.6 and 3.7 of [chapter 3](http://cacr.uwaterloo.ca/hac/about/chap3.pdf)
and section 8.4 of [chapter 8](http://cacr.uwaterloo.ca/hac/about/chap8.pdf).**  

## Discrete logarithm problem and cryptosystems

### Discrete logarithm problem

The discrete logarithm problem is usually stated as follows.
Given a multiplicative group **G** of order _r_, a generator _g_ of the group and
an element _h_ in **G**, find an integer _x_ such that _h_ = _g<sup>x</sup>_. Because this _x_
is a preimage for the exponentiation in **G**, and it takes integer values, it is
called a **discrete logarithm**.

For example, consider the prime number of 1024 bits in [OAKLEY
IETF](https://tools.ietf.org/html/rfc2412)  
_p_ = 179769313486231590770839156793787453197860296048756011706444423684197180216158519368947833795864925541502180565485980503646440548199239100050792877003355816639229553136239076508735759914822574862575007425302077447712589550957937778424442426617334727629299387668709205606050270810842907692932019128194467627007

_p_ is prime, and _q_ = (_p_-1)/2 is prime too: _p_ is a _Sophie Germain_ prime
(named after the French mathematician [Sophie Germain](https://fr.wikipedia.org/wiki/Sophie_Germain)),
also known as a _safe prime_.
_p_ has no particular form and was generated from the decimals of π:
_p_ = 2<sup>1024</sup> - 2<sup>960</sup> - 1 + 2<sup>64</sup> ·
([2<sup>894</sup> · π] + 129093)
```python
pi = 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651328230664709384460955058223172535940812848111745028410270193852110555964462294895493038196442881097566593344612847564823378678316527120190914564856692346034861
p = 2**1024 - 2**960 - 1 + 2**64 * (floor(2**894 * pi) + 129093)
```
A generator is _g_ = 2 of order _q_ (it is not of order _p_-1 but (_p_-1)/2).

Given the public parameters _p_, _q_ and _g_, it is fast to compute a modular
exponentiation _g_<sup>a</sup> mod _p_, where _a_ is an integer between 1 and _q_-1.
It is called a binary exponentiation: one reads the binary representation of _a_
and computes squares and multiplications modulo _p_ to obtain
_g<sup>a</sup>_. It requires in average _n_ squarings and _n_/2 multiplications
modulo _p_ (the numbers never grow, they are always in 1, …, _p_ -1). A simple
square-and-multiply in Python looks like:
```python
from random import randrange
p = 179769313486231590770839156793787453197860296048756011706444423684197180216158519368947833795864925541502180565485980503646440548199239100050792877003355816639229553136239076508735759914822574862575007425302077447712589550957937778424442426617334727629299387668709205606050270810842907692932019128194467627007
q = (p-1)//2
g = 2
a = randrange(1, q)
gi = g
if (a % 2) == 1:
    h = g
else:
    h = 1
ai = a // 2
while ai > 0:
    gi = g**2 % p
    if (ai % 2) == 1:
        h = (h * gi) % p
    ai = ai // 2
```
But it is a very hard mathematical problem to inverse this function, that is, to
compute _a_ when knowing only _p_, _q_, _g_ and _h_ where _h_ was computed as _g<sup>a</sup>_.

### Choice of group

The first choices of groups **G** where finite fields of two kinds:

-   Prime finite fields denoted **Z**/_p_**Z** or **F**<sub>_p_</sub> where _p_ is a prime number. This
    notation says that the arithmetic is done _modulo p_ (in Python or C, a
    reduction modulo an integer is denoted `a % b`). Considering the
    multiplication, the number 0 is discarded, all integers from 1 to _p_-1 are
    considered: {1, 2, …, _p_-1}. When multiplying or dividing two
    numbers in this set, the operation is followed by a reduction modulo _p_, so
    that the result is always in the set of numbers from 1 to _p_-1.  
	But because of mathematical algorithms to compute discrete logarithms, this
    is the same problem as for the RSA cryptosystem: the size of _p_ should be
    large (at least 2048 bits).
-   Finite fields of characteristic 2. It means, defining a group of order
    2<sup>_n_</sup>. But this is not "computing modulo 2<sup>_n_</sup>".
	It means: choosing an irreducible polynomial of degree _n_ and coefficients
    in {0, 1}. This field is denoted **F**<sub>2<sup>_n_</sup></sub> (in French)
    and GF(2<sup>_n_</sup>) for _Galois Field_ (in English).
	For example, with _n_ = 127 (it is a prime), one defines
	_f_(_x_) = _x_<sup>127</sup> + _x_ + 1 an irreducible polynomial (it does
    not factor into polynomials of smaller degree). An element in
    GF(2<sup>127</sup>) is a vector of bits 0, 1 of length 127, it corresponds
    to the coefficients of a polynomial of degree 126.
	_v_ = [v<sub>0</sub>, v<sub>1</sub>, …, v<sub>126</sub>] and
	_v_ = v<sub>0</sub> + v<sub>1</sub>_x_ + … + v<sub>126</sub>_x_<sup>126</sup>.
	The addition is done coefficient-wise, like the addition of two vectors. But
    because we are _modulo 2_, we have 1+1 = 2 = 0 mod 2, hence the coefficients
    are always in {0, 1}.
    In software (such as C), one can encode the coefficients directly as unsigned integers
    in binary representation, and the addition coefficient-wise is a XOR, there
    is no carry. To store an element of GF(2<sup>127</sup>), one needs two
    machine-words of 64 bits for example.
	In particular, the multi-precision arithmetic is much easier compared to **Z**/_p_**Z**,
    because there is no carry propagation in the arithmetic.
	For the multiplication, one way is to use look-up tables pre-computed
    according to the choice of the irreducible polynomial.


In other well-chosen groups **G**, computing a discrete logarithm is a very hard problem;
only generic algorithms apply. They have a complexity linear in the square root
of the order of the group, denoted _O_(sqrt(# **G**)), where # **G** is the
order of the group.
Nowadays, the preferred group is the group of points of an elliptic curve, and
one widely deployed example is the curve named `ed25519`.

Elliptic curves were introduced in cryptography in 1985 by Koblitz and Miller.
The addition law of the group of points is depicted in the following Figure.
The elements of this group **G** are coordinates (_x_,_y_) in
**F**<sub>_p_</sub> × **F**<sub>_p_</sub> satisfying a
curve equation of the form _y_<sup>2</sup> = _x_<sup>3</sup> + _a · x_ + _b_ with
parameters _a_, _b_ in the finite field **F**<sub>_p_</sub>.
The addition of to points _P_(_x_<sub>1</sub>, _y_<sub>1</sub>) and
Q(_x_<sub>2</sub>, _y_<sub>2</sub>) has a geometric meaning, with the
_chord and tangent_ construction. In this case the group has an additive
notation. The exponentiation takes as input an integer _x_ and a point _P_, and
computes _x · P_.

![](img/elliptic-curve-chord-tangent-1.png)

The hardness of the discrete logarithm problem (inverting the
exponentiation) on elliptic curves is well-investigated since 1985.
Computing a discrete logarithm on the group **G** of
points of an elliptic curve is done with generic techniques of complexity
_O_(sqrt(# **G**)). It has not changed since its
introduction in cryptography in 1985 (except for well-identified rare curves
that are easy to avoid). To ensure a 128-bit security level to match the
well-deployed AES-128 (with 128-bit keys), one chooses a group
on a curve of prime order of 256 bits. For example, the curve named
`ed25519` is an elliptic curve in Edwards form over the prime field **F**<sub>_p_</sub>
with _p_ = 2<sup>255</sup>-19, hence of 256 bits, and the curve has order 8 · _r_ where _r_ is a
prime of 253 bits. It offers 128 bits of security.
Elliptic curve standards (IEEE 1363, FIPS 186.2, SECG) recommend
the use of elliptic curves with group order _h_ · _r_, where _r_ is
a prime and the cofactor _h_ is an integer between 1 and 4.

In more detail, to ensure that an attack computing a discrete logarithm in a
well-chosen elliptic curve
will take at least 2<sup>_n_</sup> operations (as much as a brute-force search for
a symmetric cipher with a key of _n_ bits), one chooses a key of value near
2<sup>2*n*</sup>, that is of size 2*n* bits: then
sqrt(2<sup>2*n*</sup>) = 2<sup>_n_</sup> (usually the time unit is some basic operation
involved).


### Diffie-Hellman key exchange
The two participants are again Alice and Bob.

-   First they agree on the public parameters _p_, _q_ and a generator _g_ of order
_q_.
-   Then separately, Alice chooses her private key _a_ as a random number in the set
    {1, …, _q_-1} and computes her public key _A_ = _g<sup>a</sup>_ mod _p_, and
    Bob chooses _b_ randomly in the same set and computes _B_ = _g<sup>b</sup>_ mod _p_.
-   Alice sends _A_ to Bob and Bob sends _B_ to Alice on an insecure
    channel. None of _a_ or _b_ were sent explicitly. They are hidden in the exponents.
-   Alice computes _B<sup>a</sup>_ and Bob computes _A<sup>b</sup>_. Now they
    share the same secret data _S_ = _g<sup>ab</sup>_. It can be used to derive a
    symmetric key (to use with a symmetric cipher such as AES) and encrypt the
    following of the exchange between Alice and Bob.

Alice cannot compute Bob's private key, nor Bob can compute Alice's private key.
No one knowing the public parameters and the public data _A_ and _B_ can compute
the secret keys or the shared secret: with _A_ and _B_, one can only compute
_g_<sup>_a_+_b_</sup>, but not _g<sup>ab</sup>_.

### ElGamal, Schnorr signature, DSA

-   ElGamal encryption,  textbook (Paar Pelzl) Chapter 8 section 5 page 226,
    Handbook of Applied Cryptography [Chapter 8 section 4 (8.4)](http://cacr.uwaterloo.ca/hac/about/chap8.pdf),
    [on Wikipedia](https://en.wikipedia.org/wiki/ElGamal_encryption);
-   ElGamal signature scheme,
    textbook (Paar Pelzl) Chapter 10 section 3 page 270,
    Handbook of Applied Cryptography [Chapter 11 section 5.2 (11.5.2)](http://cacr.uwaterloo.ca/hac/about/chap11.pdf),
-   Schnorr signature, patented until February 2008,
	[on Wikipedia](https://en.wikipedia.org/wiki/Schnorr_signature)
-   Digital Signature Standard DSS
    (previously known as Digital Signature Algorithm DSA),
    textbook (Paar Pelzl) Chapter 10 section 4 page 277,
    Handbook of Applied Cryptography [Chapter 11 section 5.1 (11.5.1)](http://cacr.uwaterloo.ca/hac/about/chap11.pdf),
    [on Wikipedia](https://fr.wikipedia.org/wiki/Digital_Signature_Algorithm),
    [NIST FIPS 186-5 Section 4](https://doi.org/10.6028/NIST.FIPS.186-5),
    released on February 2023, supercedes the previous
    [NIST FIPS 186-4 Section 4](https://www.nist.gov/publications/digital-signature-standard-dss-2?pub_id=914162)
-   ECDSA (elliptic curve DSA),
    textbook (Paar Pelzl) chapter 10 section 5 (10.5 page 282),
    [NIST FIPS 186-5 Section 6](https://doi.org/10.6028/NIST.FIPS.186-5),
    released on February 2023, supercedes the previous
    [NIST FIPS 186-4 Section 6](https://www.nist.gov/publications/digital-signature-standard-dss-2?pub_id=914162)
-   EdDSA (Edwards-Curve DSA),
    [NIST FIPS 186-5 Section 7](https://doi.org/10.6028/NIST.FIPS.186-5),
    released on February 2023.

## Computing discrete logarithms

The RSA cryptosystem and the Diffie-Hellman cryptosystems were developed at the
same time in cryptography, starting in the mid 70's. Finding better algorithms
for integer factorization and discrete logarithm computation were also developed
together. The best algorithms for these two problems on large cryptographic
parameters are two variants of the Number Field Sieve algorithm, of same
complexity.
In Nancy since 2007, a software is developed and handles both
cases. Unfortunately it is poorly documented, but it holds the latest
(academic) record computations for the two problems.
The latest records are the factorization of the challenge RSA-250 (in decimal
digits, corresponding to 829 bits) in February 2020, and a discrete logarithm
computation in a prime field of 240 decimal digits (795 bits) in December 2019.

The research on discrete logarithm computation has been continuous since its use
in cryptography.

Laurent Grémy (Quarkslab) maintains a web page of record computations:
[The Discrete Logarithm Database](https://dldb.loria.fr). The following table is
obtained from this database.

|bits | decimals | Date  | Authors | Link | Comment
|:---:|:---:|:----------:|:--------------------------------------------|:--------:|:--------------------------------------------------
| 215 |  65 | 02/10/1995 | Damian Weber, Thomas Denny and Joerg Zayer | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;2cfb67d1.9510) |
| 248 |  75 | 25/04/1996 | Damian Weber, Thomas Denny and Joerg Zayer | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;40d1ce60.9604) |
| 281 |  85 | 25/09/1996 | Damian Weber, Thomas Denny and Joerg Zayer | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;1d028cd8.9609) |
| 281 |  85 | 25/11/1996 | Damian Weber, Thomas Denny and Joerg Zayer | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;30bf3766.9611) |
| 427 | 129 | 25/02/1998 | Damian Weber and Thomas Denny | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;946330c8.9802) [DOI](http://dx.doi.org/10.1007/BFb0055747) | Special _p_, Joerg Zayer was involved in a first step of the computation (see [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;596a491f.9603))
| 298 |  90 | 26/05/1998 | Antoine Joux and Reynald Lercier | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;8b79d0b5.9805) |
| 331 | 100 | 01/11/1999 | Antoine Joux and Reynald Lercier | [ECC'99, Waterloo](http://cacr.uwaterloo.ca/conferences/1999/ecc99/slides.html) [slides](http://cacr.uwaterloo.ca/conferences/1999/ecc99/lercier.ps) |
| 364 | 110 | 19/01/2001 | Antoine Joux and Reynald Lercier | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;cc6cf15b.0101) |
| 397 | 120 | 17/04/2001 | Antoine Joux and Reynald Lercier | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;2495a536.0104) |
| 431 | 130 | 18/06/2005 | Antoine Joux and Reynald Lercier | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;cc056f17.0506) |
| 448 | 135 | 22/12/2006 | Andrey Dorofeev, Denis Dygin and Dmitry Matyukhin | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;f8ddbc63.0612) |
| 530 | 160 | 05/02/2007 | Thorsten Kleinjung | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;1c737cf8.0702) |
| 596 | 180 | 11/06/2014 | Cyril Bouvier, Pierrick Gaudry, Laurent Imbert, Hamza Jeljeli and Emmanuel Thomé | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;615d922a.1406) |
| 512 |     | 20/05/2015 | David Adrian, Karthikeyan Bhargavan, Zakir Durumeric, Pierrick Gaudry, Matthew Green, J. Alex Halderman, Nadia Heninger, Drew Springall, Emmanuel Thomé, Luke Valenta, Benjamin VanderSloot, Eric Wustrow, Santiago Zanella-Béguelin and Paul Zimmermann | [DOI](http://dx.doi.org/10.1145/2810103.2813707) | Logjam attack
| 768 | 232 | 16/06/2016 | Thorsten Kleinjung, Claus Diem, Arjen Lenstra, Christine Priplata and Colin Stahlke | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;a0c66b63.1606) [DOI](http://dx.doi.org/10.1007/978-3-319-56620-7_7) |
|1024 | 309 | 10/10/2016 | Joshua Fried, Pierrick Gaudry, Nadia Heninger and Emmanuel Thomé | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;5d32aafc.1610) [DOI](http://dx.doi.org/10.1007/978-3-319-56620-7_8) | Special _p_
| 795 | 240 | 02/12/2019 | Fabrice Boudot, Pierrick Gaudry, Aurore Guillevic, Nadia Heninger, Emmanuel Thomé and Paul Zimmermann | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;fd743373.1912) [DOI](https://doi.org/10.1007/978-3-030-56880-1_3) |

### Generic algorithms of square root complexity

-  Shanks algorithm _baby-step-giant-step_ (BSGS) has complexity
   _O_(sqrt(#**G**)), and is deterministic. It requires large amount of memory.
-  A variant is a random walk in **G**, then computing a discrete logarithm is
      like finding a cycle path in a connected graph (for this, there is Floyd's
      algorithm). Applied to **G**, this is Pollard's algorithm, of complexity
      _O_(sqrt(#**G**)), probabilistic, and much less memory is needed.
-  Other time-memory trade-offs are possible, with parallel searches (parallel Pollard, Kangaroos)

**If the factorization of the order of G is known**:

-  the Pohlig-Hellman algorithm applies: it runs an independent search in each
   distinct subgroup and uses the Chinese Remainder Theorem (CRT) to combine the answers.
   Complexity: _O_(sqrt(_r_)) for each prime
   factor _r_ of #**G**, the largest prime _r_ matters: that is why **G** is
   taken of prime order (or, a tiny number times a prime).

### Index calculus algorithm 
It was introduced by Western and Miller in 1968 and Adleman in 1979.

Let _p_ be a prime number, and _q_ = (_p_ - 1)/2 (or _q_ = (_p_ - 1)/4) be a prime.
Let **G** = **Z**/_p_**Z** and let _g_ be a generator of the multiplicative
sub-group.
Let _h_ be the target: we aim at computing _x_ such that _g<sup>x</sup>_ = _h_.

1.  Get many multiplicative relations between elements of **G**:  
    _g<sup>t</sup>_ = _g_<sub>1</sub><sup>_e_<sub>1</sub></sup>
	_g_<sub>2</sub><sup>_e_<sub>2</sub></sup> ⋯
	_g_<sub>_i_</sub><sup>_e_<sub>_i_</sub></sup> mod _p_  
	where _t_ is a known integer, and _g_<sub>1</sub>, _g_<sub>2</sub>, …,
    _g_<sub>_i_</sub> are elements of **G**.
2.  Obtain a relation involving the target _h_: try many _s_ until  
    _h_ · _g<sup>s</sup>_ = _g_<sub>1</sub><sup>_e_'<sub>1</sub></sup> ·
	_g_<sub>2</sub><sup>_e_'<sub>2</sub></sup> ⋯
	_g_<sub>_i_</sub><sup>_e_'<sub>_i_</sub></sup> mod _p_
3.  Consider the exponents, that is, takes the logarithms in basis _g_:
	one obtains linear relations modulo _p_ - 1:  
	_t_ = _e_<sub>1</sub> log<sub>_g_</sub> _g_<sub>1</sub> + 
          _e_<sub>2</sub> log<sub>_g_</sub> _g_<sub>2</sub> + …
		  _e_<sub>_i_</sub> log<sub>_g_</sub> _g_<sub>_i_</sub> mod _p_ - 1  
    …  
	log<sub>_g_</sub>_h_ + _s_ = _e_'<sub>1</sub> log<sub>_g_</sub> _g_<sub>1</sub> + 
          _e_'<sub>2</sub> log<sub>_g_</sub> _g_<sub>2</sub> + …
		  _e_<sub>_i_'</sub> log<sub>_g_</sub> _g_<sub>_i_</sub> mod _p_ - 1
4.  Solve a linear system modulo _p_ - 1
5.  get _x_ = log<sub>_g_</sub> _h_

Numerical example: _p_ = 1109, _q_ = (_p_ - 1)/4 = 277 is prime.
Consider the prime integers 2, 3, 5, 7, 11, 13 for the _g_<sub>_i_</sub>.  

1.  Consider successive integers _t_ starting at 2 and until six
    relations are found, because there are six integers _g_<sub>_i_</sub> of
    unknown logarithm. We start with _g_ = 2 to ensure that the logarithms are in basis
    _g_.  
	_g_<sup>1</sup> = 2  
	_g_<sup>13</sup> = 429 = 3 · 11 · 13  
	_g_<sup>16</sup> = 105 = 3 · 5 · 7  
	_g_<sup>21</sup> = 33  = 3 · 11  
	_g_<sup>44</sup> = 1029= 3 · 7<sup>3</sup>  
	_g_<sup>72</sup> = 325 = 5<sup>2</sup> · 13  
2.  With the target _h_ = 777, one finds that _g_<sup>10</sup> · _h_ = 495 =
    3<sup>2</sup> · 5 · 11 mod _p_.
3.  Solving the system modulo _q_ then _p_ - 1 gives
    log<sub>_g_</sub> 2 = 1,
    log<sub>_g_</sub> 3 = 219,
    log<sub>_g_</sub> 5 = 594,
    log<sub>_g_</sub> 7 = 311,
    log<sub>_g_</sub> 11 = 910,
    log<sub>_g_</sub> 13 = 1100.
4.  Finally, log<sub>_g_</sub> _h_ = -10 + 2 log<sub>_g_</sub> 3 +
    log<sub>_g_</sub> 5 + log<sub>_g_</sub> 11 = 824 mod _p_ - 1.
	Indeed, one can check that _g_<sup>824</sup> = 777 mod _p_.


### Quadratic Sieve, Number Field Sieve
In 1986, Coppersmith, Odlyzko, and Schroeppel published the quadratic sieve.
Is introduces a technique of
_sieving_ to avoid a complete factorization of non-promising numbers. It detects
the smooth integers. Then, they can be factored into prime factors. The
non-smooth integers are not factorized at all. However, it requires the
space to store in memory (in RAM) all the numbers.

In 1985, ElGamal designed an algorithm to compute discrete logarithms in
GF(_p_<sup>2</sup>) with two quadratic number fields.
In 1986, Coppersmith, Odlyzko, and Schroeppel applied it to GF(_p_).
It allowed a more mathematical way to look at the index calculus technique.
It introduces the use of a quadratic number field. For example, 
the complex numbers _a_ + _b_ i where i<sup>2</sup> = -1, and with 
_a_ and _b_ rational numbers (in **Q**), is a number field.

It was then possible to generalize the algorithm to more general number fields.
The idea was applied to the factorization of integers.
Right after, in 1993, Gordon published the number field sieve algorithm for
computing discrete logarithms in prime fields. The complexity was improved by
Schirokauer the same year, and it is nowadays the same complexity.
The algorithm was continuously improved in practice until now, but the
asymptotic complexity did not change.

### Choosing key-sizes

The website [keylength.com](https://www.keylength.com/)
lists the recommendations from different agencies.

## Attacks on discrete-logarithm based cryptosystems

### Sony Play-Station 3 (PS3) hacking
It was a problem of bad randomness in the ECDSA signature
[Slides of the talk at CCC 122-134](https://fahrplan.events.ccc.de/congress/2010/Fahrplan/attachments/1780_27c3_console_hacking_2010.pdf)
(Chaos Communication Congress, every year in Germany).
The same random token was used to sign everything. It means with two
public signatures, one can recover the private key and then sign anything with
this private key.

#### ECDSA signature
(From textbook Paar Pelzl, Chapter 10 section 5)

ECDSA (Elliptic Curve Digital Signature Algorithm) needs an elliptic curve _E_
defined over a prime field GF(_p_) (it works for other types of fields, in
particular GF(2<sup>_n_</sup>), Koblitz curves and binary curves).
Assume the curve has a sub-group of large prime order _q_ and a generator is
given as a point _G_(_x_<sub>_G_</sub>,_y_<sub>_G_</sub>).

**Public Parameters**

-   _p_
-   Elliptic curve _E_ (coefficients of the curve equation)
-   group order _q_ (prime)
-   generator of order _q_: _G_=(_x_<sub>_G_</sub>,_y_<sub>_G_</sub>)

**Key generation**

1.  Choose a random integer _d_ in {1, …, _q_ - 1}
2.  Compute the point _P_ = _d_ · _G_

-   Public key: (_p_, _q_, _E_ (coefficients), _G_, _P_)
-   Private key: (_d_)

**Signing** of a message _m_:

1.  Choose an ephemeral private key as an integer _k<sub>e</sub>_ in {1, …, _q_ - 1}.
2.  Compute the point _R_ = _k<sub>e</sub>_ · _G_
3.  Let _r_ = _x<sub>R</sub>_ the _x_-coordinate of the point _R_.
4.  Compute _s_ = (_H_(_m_) + _d_ · _r_) · _k<sub>e</sub>_<sup>-1</sup> mod _q_
    where _H_ is a cryptographic hash function whose output is at least of the same
    length as of _q_ (for example, 256 bits or 32 bytes).  
    The signature is (_r_, _s_).

**Verification**

1.  Compute _w_ = _s_<sup>-1</sup> mod _q_
2.  Compute _u_<sub>1</sub> = _w_ · _H_(_m_) mod _q_ (with the same hash function _H_)
3.  Compute _u_<sub>2</sub> = _w_ · _r_ mod _q_
4.  Compute _Q_ = _u_<sub>1</sub> · _G_ + _u_<sub>2</sub> · _P_ (a point on _E_)
5.  if _x<sub>Q</sub>_ = _r_ mod _q_ then the signature is valid,  
    otherwise the signature is invalid.

#### PS3 problem

The hackers discovered that the same ephemeral key _k<sub>e</sub>_ was used to
sign different things. They were able to obtain the data for distinct messages,
say _m_<sub>1</sub> and _m_<sub>2</sub>. It consists in

-   (_r_, _s_<sub>1</sub> = (_H_(_m_<sub>1</sub>) + _d_ · _r_) · _k<sub>e</sub>_<sup>-1</sup> mod _q_)
-   (_r_, _s_<sub>2</sub> = (_H_(_m_<sub>2</sub>) + _d_ · _r_) · _k<sub>e</sub>_<sup>-1</sup> mod _q_)

One wants to recover the private key _d_. Observe that  
_s_<sub>1</sub> - _s_<sub>2</sub> mod _q_
= (_H_(_m_<sub>1</sub>) + _d_ · _r_) · _k<sub>e</sub>_<sup>-1</sup> - (_H_(_m_<sub>2</sub>) + _d_ · _r_) · _k<sub>e</sub>_<sup>-1</sup>
= (_H_(_m_<sub>1</sub>) - _H_(_m_<sub>2</sub>)) · _k<sub>e</sub>_<sup>-1</sup>
mod _q_  
The secret key _d_ vanished! We can publicly compute
_h_ = _H_(_m_<sub>1</sub>) - _H_(_m_<sub>2</sub>) mod _q_, then recover the
(actually non-ephemeral) key _k<sub>e</sub>_.

-   _h_ = _H_(_m_<sub>1</sub>) - _H_(_m_<sub>2</sub>) mod _q_
-   _k<sub>e</sub>_ = _h_ / (_s_<sub>1</sub> - _s_<sub>2</sub>) mod _q_
-   _d_ = (_s_<sub>1</sub> · _k<sub>e</sub>_ - _H_(_m_<sub>1</sub>)) / _r_ mod _q_

Knowing the manufacturer's private key _d_ allows to sign non-legitimate
documents (software, games for the PS3), and the signature will be accepted as
a valid signature by any verifier.

### Weak Diffie-Hellman and the logjam attack (2015)
[Web page](https://weakdh.org/)

-   David Adrian, Karthikeyan Bhargavan, Zakir Durumeric, Pierrick Gaudry, Matthew
    Green, J. Alex Halderman, Nadia Heninger, Drew Springall, Emmanuel Thomé,
    Luke Valenta, Benjamin VanderSloot, Eric Wustrow, Santiago
    Zanella-Béguelin, and Paul Zimmermann.
    **Imperfect forward secrecy: How Diffie-Hellman fails in practice**.
    In _22nd ACM Conference on Computer and Communications Security_,
    October 2015.
    [url](https://weakdh.org/).

This attack was inspired by the [FREAK attack](https://freakattack.com/).
The FREAK attack consists in an active MITM downgrade attack to change the `RSA`
ciphersuite to the `RSA_EXPORT` ciphersuite in TLS, where the modulus is much smaller.
A factorization of a 512-bit RSA modulus is required for the attacker to take
over the TLS connection.

In the Weak DH and logjam attack, the MITM attack also starts by convincing both
parties to negociate `DHE_EXPORT` instead of `DHE` (Diffie-Hellman Ephemeral key
exchange) as cipher suite in the TLS handshake.

Below is a summary of a regular **TLS 1.2 handshake** between a client (web
browser) and a server.
Resources can be found at

-   [RFC 5246 (TLS 1.2)](https://datatracker.ietf.org/doc/html/rfc5246)
-   [TLS seminar](https://tlseminar.github.io/first-few-milliseconds/)

![TLS 1.2 Handshake](img/tls-12-handshake.png)

Below is a summary of the MITM Logjam attack on a TLS 1.2 handshake between a
client and a server.

![MITM Logjam attack](img/tls-12-logjam-attack.png)

### Weak keys in the Moscow internet voting system
[Web page](https://members.loria.fr/PGaudry/moscow/)

## Discrete logarithm computation in finite fields **F**<sub>2<sup>_n_</sup></sub> = GF(2<sup>_n_</sup>) and **F**<sub>3<sup>_m_</sup></sub> = GF(3<sup>_m_</sup>)

For better hardware efficiency, finite fields **F**<sub>2<sup>_n_</sup></sub> =
GF(2<sup>_n_</sup>) were introduced in cryptography. But already in 1984, it was
known that computing a discrete logarithm was easier in these fields, that is,
for a same size (a field GF(2<sup>_n_</sup>) has _n_ bits, a prime field GF(_p_)
has size the number of bits of _p_), it was much faster to compute a discrete
logarithm in GF(2<sup>_n_</sup>), because these kind of fields have more structure.
In 1984, Blake, Fuji-Hara, Mullin, Vanstone in two papers, and Coppersmith in a
third paper showed how to compute discrete logarithms in GF(2<sup>127</sup>).

-  Blake, I.F., Fuji-Hara, R., Mullin, R.C., Vanstone, S.A.: Computing logarithms
   in finite fields of characteristic two. SIAM Journal on Algebraic Discrete
   Methods  **5**(2),  pages 276-285 (1984).
   DOI [10.1137/0605029](https://doi.org/10.1137/0605029)
-  Blake, I.F., Mullin, R.C., Vanstone, S.A.: Computing logarithms in
   GF(2<sup>_n_</sup>). In: Blakley, G.R., Chaum, D. (eds.) CRYPTO'84. LNCS,
   vol. 196, pp. 73-82. Springer, Heidelberg (Aug 1984).
   DOI [10.1007/3-540-39568-7_8](https://doi.org/10.1007/3-540-39568-7_8)
-  Coppersmith, D.: Fast evaluation of logarithms in fields of characteristic two.
   IEEE Transactions on Information Theory  **30**(4),  pages 587-594 (1984).
   DOI [10.1109/TIT.1984.1056941](https://doi.org/10.1109/TIT.1984.1056941)

The following graph shows the record computations in finite fields of the form
GF(2<sup>_n_</sup>) and GF(3<sup>_m_</sup>), except for the latest record in 
GF(2<sup>30750</sup>) that is outside of the scale. The data is extracted from
[dldb](https://dldb.loria.fr).

![](img/records-DL-GF2n-GF3m-1.png)

field |bits | decimals | Date  | Authors | Link | Comment
:----:|:---:|:---:|:----------:|:--------------------------------------------|:--------:|:--------------------------------------------------
2<sup>30750</sup>|30750 |9257 | 10/07/2019 | Robert Granger, Thorsten Kleinjung, Arjen Lenstra, Benjamin Wesolowski and Jens Zumbrägel | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;62ab27f0.1907) [ePrint](https://eprint.iacr.org/2020/965) | Kummer extension
3<sup>3054</sup> | 4841 |2310 | 18/07/2016 | Gora Adj, Isaac Canales-Martinez, Nareli Cruz-Cortés, Alfred Menezes, Thomaz Oliveira, Luis Rivera-Zamarripa and Francisco Rodriguez-Henriquez | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;65bedfc8.1607) [DOI](http://dx.doi.org/10.3934/amc.2018044) | 
2<sup>1279</sup> | 1279 | 386 | 17/10/2014 | Thorsten Kleinjung | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;256db68e.1410) | Prime extension
3<sup>2395</sup> | 3796 |1812 | 15/09/2014 | Antoine Joux and Cécile Pierrot | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;1ff78abb.1409) [DOI](http://dx.doi.org/10.1007/978-3-662-45611-8_20) | 
2<sup>9234</sup> | 9234 |2780 | 31/01/2014 | Robert Granger, Thorsten Kleinjung and Jens Zumbrägel | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;9aa2b043.1401) | Kummer extension
2<sup>4404</sup> | 4404 |1326 | 30/01/2014 | Robert Granger, Thorsten Kleinjung and Jens Zumbrägel | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;23651c2.1401) [DOI](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;23651c2.1401) |
3<sup>978</sup>  | 1551 | 741 | 26/01/2014 | Gora Adj, Alfred Menezes, Thomaz Oliveira and Francisco Rodriguez-Henriquez | [DOI](http://dx.doi.org/10.1007/978-3-319-16277-5_1) | 
3<sup>822</sup>  | 1303 | 622 | 26/01/2014 | Gora Adj, Alfred Menezes, Thomaz Oliveira and Francisco Rodriguez-Henriquez | [DOI](http://dx.doi.org/10.1007/978-3-319-16277-5_1) | 
2<sup>3164</sup> | 3164 | 953 | 21/08/2013 | Faruk Göloğlu, Robert Granger, Gary McGuire and Jens Zumbrägel | [DOI](http://dx.doi.org/%2010.1007/978-3-642-40084-1_7) | Kummer extension
2<sup>6168</sup> | 6168 |1857 | 21/05/2013 | Antoine Joux | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;49bb494e.1305) | Kummer extension
2<sup>6120</sup> | 6120 |1843 | 11/04/2013 | Faruk Göloğlu, Robert Granger, Gary McGuire and Jens Zumbrägel | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;fe9605d9.1304) | Kummer extension
2<sup>809</sup>  |  809 | 244 | 09/04/2013 | Razvan Barbulescu, Cyril Bouvier, Jérémie Detrey, Pierrick Gaudry, Hamza Jeljeli, Emmanuel Thomé, Marion Videau and Paul Zimmermann | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;6ac1ef5b.1304) [DOI](http://dx.doi.org/10.1007/978-3-642-54631-0_13) | Prime extension
2<sup>4080</sup> | 4080 |1229 | 22/03/2013 | Antoine Joux | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;71e65785.1303) | Kummer extension
2<sup>1971</sup> | 1971 | 594 | 19/02/2013 | Faruk Göloğlu, Robert Granger, Gary McGuire and Jens Zumbrägel | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;f7755cbe.1302) [DOI](http://dx.doi.org/%2010.1007/978-3-642-40084-1_7) | Kummer extension
2<sup>1778</sup> | 1778 | 536 | 11/02/2013 | Antoine Joux | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;7d4dd9a6.1302) | Kummer extension
2<sup>619</sup>  |  619 | 187 | 29/10/2012 | Razvan Barbulescu, Cyril Bouvier, Jérémie Detrey, Pierrick Gaudry, Hamza Jeljeli, Emmanuel Thomé, Marion Videau and Paul Zimmermann | [ECC2012](http://ecc.2012.rump.cr.yp.to/b8468eca1dd517ac47734b85ff43c699.pdf) | Prime extension
3<sup>582</sup>  |  923 | 441 | 18/06/2012 | Takuya Hayashi, Takeshi Shimoyama, Naoyuki Shinohara and Tsuyoshi Takagi | [DOI](http://dx.doi.org/10.1007/978-3-642-34961-4_5) | 
3<sup>426</sup>  |  676 | 323 | 26/05/2010 | Takuya Hayashi, Naoyuki Shinohara, Lihua Wang, Shin’ichiro Matsuo, Masaaki Shirase and Tsuyoshi Takagi | [DOI](http://dx.doi.org/10.1007/978-3-642-13013-7_21) | 
2<sup>613</sup>  |  613 | 185 | 23/09/2005 | Antoine Joux and Reynald Lercier | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;48e40c30.0509) | Prime extension
2<sup>607</sup>  |  607 | 183 | 23/09/2005 | Antoine Joux and Reynald Lercier | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;48e40c30.0509) | Prime extension
2<sup>607</sup>  |  607 | 183 | 23/02/2002 | Emmanuel Thomé | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;c706a5f9.0202) | Prime extension
2<sup>521</sup>  |  521 | 157 | 25/09/2001 | Antoine Joux and Reynald Lercier | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;6e71a967.0109) | Prime extension
2<sup>401</sup>  |  401 | 121 | 16/08/1992 | Daniel Gordon and Kevin McCurley | [DOI](http://dx.doi.org/10.1007/3-540-48071-4_22) | Prime extension
2<sup>313</sup>  |  313 |  95 | 16/08/1992 | Daniel Gordon and Kevin McCurley | [DOI](http://dx.doi.org/10.1007/3-540-48071-4_22) | Prime extension
2<sup>227</sup>  |  227 |  69 | 16/08/1992 | Daniel Gordon and Kevin McCurley | [DOI](http://dx.doi.org/10.1007/3-540-48071-4_22) | Prime extension
2<sup>127</sup>  |  127 |  39 | 01/07/1984 | Don Coppersmith | [DOI](http://dx.doi.org/10.1007/3-540-48071-4_22) | Prime extension

(The following summary of historical steps is from Chapter 9 _Discrete
Logarithms_ in _Guide to pairing-based cryptography_), a free archive is on [HAL](https://hal.inria.fr/hal-01420485v2).

In 1984, Blake, Fuji-Hara, Mullin, and Vanstone proposed
a dedicated implementation of Adleman's algorithm in GF(2<sup>127</sup>).
They introduced the idea of _systematic equations_, a very efficient way to
generate relations thanks to the structure of the field (in that field,
(_x_+_y_)<sup>2</sup> = _x_<sup>2</sup> + _y_<sup>2</sup> modulo 2) and
_initial splitting_ (the name of their technique was introduced later).
Their idea works for finite fields like GF(2<sup>_n_</sup>) and _n_ is close to a
power of 2 (e.g., 127 = 2<sup>7</sup> - 1). 
The asymptotic complexity of their method was exp(sqrt((2+o(1)) ln
2<sup>_n_</sup> ln ln 2<sup>_n_</sup>)). 
In the same year, Blake, Mullin, and Vanstone
proposed an improved algorithm known as the Waterloo algorithm (it is a city in
Ontario, Canada where is a famous university for computer science and electrical
engineering, the author of the _Handbook of applied cryptography_ is professor
at the university of Waterloo). 
Odlyzko computed the asymptotic complexity of this algorithm in a paper
published at the conference Eurocrypt'84 [DOI](https://link.springer.com/chapter/10.1007/3-540-39757-4_20).
Coppersmith published the first record computation of a discrete logarithm in
GF(2<sup>127</sup>) in 1984 and designed an algorithm of much better asymptotic
complexity:
exp((_c_+o(1)) (ln 2<sup>_n_</sup>)<sup>1/3</sup> (ln ln 2<sup>_n_</sup>)<sup>2/3</sup>)
for a constant _c_ between 1.526
and 1.587. His idea was generalized to other finite fields but with a larger
constant _c_.
There was no significant improvement until the rise of finite fields coming
with _pairing-friendly curves_ in 2000 (see the following sections).
The latest record computation took place in GF(2<sup>30750</sup>) in 2019, that
is, the discrete logarithm problem is _easy_ in fields GF(2<sup>_n_</sup>) and
GF(3<sup>_m_</sup>) for _n_, _m_ composite, and
it is impossible to build any secure cryptosystem in these cases.

In the case of pairing-friendly curves, with GF(2<sup>_n_</sup>), the extension degree
_n_ is a multiple of 4, and with GF(3<sup>_m_</sup>), _m_ is a multiple of 6.
This is an important property that made a big difference with the classical case
where _n_ and _m_ are prime.

In 2010, the record computation was in GF(3<sup>6 · 71</sup>) of 676 bits (323
decimal digits), made by a Japanese team. In 2012, they improved their record to
GF(3<sup>6 · 97</sup>) of 923 bits (441 decimal digits).
In addition to the improvements of the techniques, the record was also made
possible thanks to the increasing computing power.
This announcement had a quite important effect over the community at
that time, probably because the broken curve was the one
used in the initial paper on short signatures from pairings
(Asiacrypt'01, Boneh Lynn Shacham).
The broken finite field GF(3<sup>6 · 97</sup>) was the
target field of a pairing-friendly elliptic curve defined over
GF(3<sup>97</sup>), considered safe in 2001 for 80-bit security
implementations.
Here are the press release (accessible to non-specialists) and the publications
(more technical).

-  Fujitsu Laboratories, NICT, and Kyushu University. DL record in
   **F**<sub>3<sup>6 · 97</sup></sub> of 923 bits (278 decimal digits). NICT press
   release, June 18, 2012. [NICT page](http://www.nict.go.jp/en/press/2012/06/18en-1.html)
-  Takuya Hayashi, Naoyuki Shinohara, Lihua Wang, Shin'ichiro Matsuo, Masaaki
   Shirase, and Tsuyoshi Takagi.
   Solving a 676-bit discrete logarithm problem in GF(3<sup>6*n*</sup>).
   In P. Q. Nguyen and D. Pointcheval, editors, _Public Key
   Cryptography - PKC 2010_, volume 6056 of LNCS, pp. 351-367.
   Springer, Heidelberg, 2010.
   DOI [10.1007/978-3-642-13013-7_21](http://dx.doi.org/10.1007/978-3-642-13013-7_21)
-  Takuya Hayashi, Takeshi Shimoyama, Naoyuki Shinohara, and Tsuyoshi Takagi.
   Breaking pairing-based cryptosystems using eta<sub>_T_</sub> pairing over
   GF(3<sup>97</sup>).
   In X. Wang and K. Sako, editors, _ASIACRYPT 2012_, vol. 7658 of LNCS,
   pp. 43--60. Springer, Heidelberg, 2012.
   DOI [10.1007/978-3-642-34961-4_5](http://dx.doi.org/10.1007/978-3-642-34961-4_5)

At Christmas 2012, Joux proposed a conjectured heuristic algorithm
of asymptotic complexity with an exponent 1/4 instead of 1/3:
exp((_c_+o(1)) (ln 2<sup>_n_</sup>)<sup>1/4</sup> (ln ln 2<sup>_n_</sup>)<sup>3/4</sup>)
and announced two records of much larger size.
The years 2013 and 2014 were very intense in improvements and record
computations and in 2014, two algorithms of _quasi polynomial time_ complexity were
published.
A major mathematical proof required in the algorithms was published in 2019.
And the latest record computation is extremely large, of 30750 bits.

# Pairings

## What is a pairing?

A pairing (_couplage_ in French) is a bilinear map that takes as input two
points _P_=(_x_<sub>1</sub>, _y_<sub>1</sub>), _Q_=(_x_<sub>2</sub>,
_y_<sub>2</sub>) on an elliptic curve _E_ defined over a field
**F**<sub>_q_</sub>, and outputs a value in the finite field
**F**<sub>_q_<sup_k_</sup></sub>. It is denoted  
_e_: **G**<sub>1</sub> × **G**<sub>2</sub> → **G**<sub>3</sub>  
where **G**<sub>1</sub> and **G**<sub>2</sub> are subgroups of _E_ and
**G**<sub>3</sub> is a subgroup of **F**<sub>_q_<sup_k_</sup></sub>.
The map is

-   bilinear: _e_(_P_<sub>1</sub> + _P_<sub>2</sub>, _Q_) =
    _e_(_P_<sub>1</sub>, _Q_) · _e_(_P_<sub>2</sub>, _Q_) and  
    _e_(_P_, _Q_<sub>1</sub> + _Q_<sub>2</sub>) =
    _e_(_P_, _Q_<sub>1</sub>) · _e_(_P_, _Q_<sub>2</sub>);
-   non-degenerate: one cannot have _e_(_P_,_Q_) = 1 unless one of _P_, _Q_ is
    the neutral element of the curve;
-   efficiently computable.

In practice, the important property is the following: one can swap the discrete
logarithm values and multiply them in the exponent: with two points _P_ and _Q_
and integers _a_ and _b_, we have the equality:  
_e_(_aP_, _bQ_) = _e_(_bP_, _aQ_) = _e_(_P_, _Q_)<sup>_ab_</sup>


## Pairings in cryptography: 1993 and 2001

### 1993

Elliptic curves were introduced in 1985 for public-key cryptography. To use a
curve as a group **G** for Diffie-Hellman (or any other discrete logarithm based
cryptosystem), on needs to know the order of the curve.
For a finite field **Z**/_p_**Z** where _p_ is a prime, the order of the
multiplicative group is _p_ -1 because one removes zero and works with the set
of integers {1, 2, 3, …, _p_ - 1}.
But for a curve, it is not the same. Its order can be any integer between _p_ -
2 · sqrt(_p_) and _p_ + 2 · sqrt(_p_). At that time, it was very hard to compute the
exact number of points on the curve, and to bypass this difficulty,
_supersingular_ curves were used instead.

Supersingular curves have an equation of the form

-   _E_<sub>_p_</sub>: _y_<sup>2</sup> = _x_<sup>3</sup> + _x_ over a prime field
    **F**<sub>_p_</sub> where _p_ modulo 4 is equal to 3, its order is _p_ + 1
    and **G**<sub>3</sub> = **F**<sub>_p_<sup>2</sup></sub>;
-   _E_<sub>_p_</sub>: _y_<sup>2</sup> = _x_<sup>3</sup> + 1 over a prime field
    **F**<sub>_p_</sub> where _p_ modulo 3 is equal to 2, its order is _p_ + 1
	and **G**<sub>3</sub> = **F**<sub>_p_<sup>2</sup></sub>;
-   _E_<sub>3,1</sub>: _y_<sup>2</sup> = _x_<sup>3</sup> - _x_ + 1 or
    _E_<sub>3,-1</sub>: _y_<sup>2</sup> = _x_<sup>3</sup> - _x_ - 1 over a
    field **F**<sub>3<sup>_m_</sup></sub> with odd _m_.
    Its order is
    3<sup>_m_</sup> + 3<sup>(_m_+1)/2</sup> + 1 or
    3<sup>_m_</sup> - 3<sup>(_m_+1)/2</sup> + 1
	(depending on the choice of the coefficient 1 or -1)
	and **G**<sub>3</sub> = **F**<sub>3<sup>6 · _m_</sup></sub>;
-   _E_<sub>2,1</sub>: _y_<sup>2</sup> + _y_ = _x_<sup>3</sup> + _x_ + 1
    or _E_<sub>2,0</sub>: _y_<sup>2</sup> + _y_ = _x_<sup>3</sup> + _x_
    over a field **F**<sub>2<sup>_n_</sup></sub> with odd _n_.
    Its order is
    2<sup>_n_</sup> + 2<sup>(_n_+1)/2</sup> + 1 or
    2<sup>_n_</sup> - 2<sup>(_n_+1)/2</sup> + 1
	and **G**<sub>3</sub> = **F**<sub>2<sup>4 · _n_</sup></sub>.

These curves were easy to generate: for curves over **F**<sub>_p_</sub>, the aim
is to find a prime _p_ such that _p_+1 = 4 · _r_ with _r_ a prime for the first
curve, and _p_+1 = 6 · _r_ with _r_ prime for the second curve.
For the curves over **F**<sub>2<sup>_n_</sup></sub> and
**F**<sub>3<sup>_m_</sup></sub>, the aim is to find _n_, or _m_ such that the
order of the curve is prime, or a tiny factor times a prime.
For this, this is well documented that the order of the curve is the an
_Aurifeuillean factor_:

-   Define _P_<sub>6</sub>(_x_) = _x_<sup>2</sup> - _x_ + 1 the 6-th cyclotomic polynomial,
    we have _P_<sub>6</sub>(_x_) · (_x_+1) = _x_<sup>3</sup>+1,
	_P_<sub>6</sub>(3*x*<sup>2</sup>) =
	(3*x*<sup>2</sup> - 3*x* + 1) · (3*x*<sup>2</sup> + 3*x* + 1),
	and _P_<sub>6</sub>(3*x*<sup>2</sup>) divides
	(3*x*<sup>2</sup>)<sup>3</sup>+1.
	Now one looks at the [Cunningham tables](https://homes.cerias.purdue.edu/~ssw/cun/)
	and search for a known factorization of 3<sup>3(2*b*+1)</sup>+1.
-   Define _P_<sub>4</sub>(_x_) = _x_<sup>2</sup> + 1 the 4-th cyclotomic polynomial,
    we have
    _P_<sub>4</sub>(2*x*<sup>2</sup>) =
	(2*x*<sup>2</sup> - 2*x* + 1) · (2*x*<sup>2</sup> + 2*x* + 1),
	and _P_<sub>4</sub>(2*x*<sup>2</sup>) = (2*x*<sup>2</sup>)<sup>2</sup>+1.
	Now one looks at the [Cunningham tables](https://homes.cerias.purdue.edu/~ssw/cun/)
	and search for a known factorization of 2<sup>2(2*a*+1)</sup>+1.

In 1993, Menezes, Okamoto and Vanstone with the Weil pairing then Frey and Rück
in 1994 with the Tate pairing showed that it is possible to compute discrete
logarithms in a finite field **F**<sub>_p_<sup>2</sup></sub> instead of an
elliptic curve, that is, transferring the computation in **G**<sub>3</sub>.
But if it is much easier to compute a discrete logarithm in **G**<sub>3</sub>,
then this is a big failure in the system.
Indeed, it was known from 1984 that a discrete logarithm in fields of the form
**F**<sub>2<sup>_n_</sup></sub> is much easier (for a same total size).

For **F**<sub>_p_<sup>2</sup></sub>, this is a problem of size of _p_.
To ensure a security level of 80 bits of security on the elliptic curve (in
1993), a prime _p_ of 160 bits was chosen, and a curve _E_ of order _q_ of 160 bits.
If the curve is supersingular, then **G**<sub>3</sub> = **F**<sub>_p_<sup>2</sup></sub>
and this field has size 320 bits. The algorithm of ElGamal applies (a quadratic
sieve-like algorithm) and has complexity in
exp(sqrt(48 · ln _p_ · ln ln _p_)). It was not practical at that time, but in
1993, another algorithm was found for prime fields **F**<sub>_p_</sub> of much
better complexity (Gordon,
exp((64/9 · ln _p_ · (ln ln _p_)<sup>2</sup>)<sup>1/3</sup>))
and it was only a matter of time before his algorithm was adapted to fields
**F**<sub>_p_<sup>2</sup></sub> (Joux Lercier Smart Vercauteren in 2006,
and in 2015, Barbulescu Gaudry Guillevic Morain showed that computing a discrete
logarithm in **F**<sub>_p_<sup>2</sup></sub> is easier than in a prime field
**F**<sub>_q_</sub> of same total size).

The big impact was for supersingular curves over **F**<sub>2<sup>_n_</sup></sub>
and **F**<sub>3<sup>_m_</sup></sub>.
In that case, Coppersmith's algorithm applies _and is practical and implemented
since 1984_.
Its complexity is
exp((32/9 · ln 2<sup>_l_</sup> · ln ln 2<sup>_l_</sup>)<sup>1/3</sup>)
and replacing _l_ by 4*n*, one obtains roughly 2<sup>56</sup>, that is, the
security is 56 bits (like the old DES) instead of 80 bits.

As a consequence, supersingular curves were avoided.

-   Menezes, A., Okamoto, T., Vanstone, S.A.: Reducing elliptic curve logarithms to
    logarithms in a finite field. IEEE Transactions on Information Theory  **39**(5),
    pages 1639-1646 (1993).
    DOI [10.1109/18.259647](https://doi.org/10.1109/18.259647)
-   Frey, G., Rück, H.G.: A remark concerning _m_-divisibility and the discrete
    logarithm in the divisor class group of curves. Mathematics of Computation
    **62**(206),  pages 865-874 (1994).
	DOI [10.1090/S0025-5718-1994-1218343-6](https://doi.org/10.1090/S0025-5718-1994-1218343-6)

### 2001

In the 90's, researchers worked on an efficient way of computing pairings. In
2000, Joux, based on previous works, was able to obtain an algorithm to compute very
efficiently a pairing, in the spirit of a scalar multiplication on the curve.
Many improvements followed, and in 2005, a very popular curve was found by
Barreto and Naehrig (BN curve).

In 2000, Joux together with his efficient algorithm, proposed a key-exchange
like a Diffie-Hellman key-exchange but with three participants.
His idea needs an elliptic curve with a pairing. The important point is the
choice of the curve: to ensure the same security level in the three groups
**G**<sub>1</sub>, **G**<sub>2</sub> and **G**<sub>3</sub>.

- Joux' key exchange
- Boneh Franklin Identity based encryption
- Boneh Lynn Shacham short signature [BLS](https://en.wikipedia.org/wiki/BLS_digital_signature)

#### Joux' tri-partite key exchange.

First, the three participants agree on public parameters: an elliptic curve _E_
with an efficient pairing, a base point (generator) _P_ for **G**<sub>1</sub>, a
base point _Q_ for **G**<sub>2</sub>. The three groups **G**<sub>_i_</sub> are
all of prime order _r_.

1.  
    -   Alice chooses a random integer _a_ in {1, …, _r_ - 1}, computes _aP_
        and sends it to Bob and Charlie;
    -   Bob chooses a random integer _b_ in {1, …, _r_ - 1}, computes _bP_,
        _bQ_ and send _bP_ to Alice and _bQ_ to Charlie;
    -   Charlie chooses a random integer _c_ in {1, …, _r_ - 1}, computes _cQ_
        and sends it to Alice and Bob;
2.  
    -   Alice computes _e_(_bP_, _cQ_)<sup>_a_</sup> = _e_(_P_, _Q_)<sup>_abc_</sup>,
    -   Bob computes _e_(_aP_, _cQ_)<sup>_b_</sup> = _e_(_P_, _Q_)<sup>_abc_</sup>,
    -   Charlie computes _e_(_aP_, _bQ_)<sup>_c_</sup> = _e_(_P_, _Q_)<sup>_abc_</sup>.
3.  They all share the secret data _e_(_P_, _Q_)<sup>_abc_</sup>.

The security relies on the hardness of the discrete logarithm problem in the
three groups **G**<sub>1</sub>, **G**<sub>2</sub>, **G**<sub>3</sub> and on the
hardness of inverting the pairing.


## Pairings with curves over fields GF(2<sup>_n_</sup>) (and GF(3<sup>_m_</sup>)), rise and fall
(From Steven Galbraith's talk title
[_The fall and rise and fall and rise of supersingular elliptic curves (in cryptography)_](https://www.math.auckland.ac.nz/~sgal018/Silverberg.pdf)

As we have seen in the previous section, supersingular curves in small
characteristic were avoided in the 90's because of the Menezes-Okamoto-Vanstone
attack.

## Pairings with curves over fields **Z**/_p_**Z** = **F**<sub>_p_</sub> = GF(_p_)

See this page [Pairing-friendly curves](https://members.loria.fr/AGuillevic/pairing-friendly-curves/)
for a short-list of interesting curves for cryptography.

## Computing Discrete logarithms in **F**<sub>_p<sup>n</sup>_</sub> = GF(_p<sup>n</sup>_)

![Record computations of discrete logarithms in GF(_p_<sup>_n_</sup>)](img/records-DL-GFqn-1.png)

field |bits | decimals | Date  | Authors | Link | Comment
:----:|:---:|:---:|:----------:|:--------------------------------------------|:--------:|:--------------------------------------------------
_p_<sup>2</sup> | 595 || 29/04/2015 | Razvan Barbulescu, Pierrick Gaudry, Aurore Guillevic and François Morain | [DOI](https://dx.doi.org/10.1007/978-3-662-46800-5_6) |
_p_<sup>2</sup> | 529 || 24/06/2014 | Razvan Barbulescu, Pierrick Gaudry, Aurore Guillevic and François Morain | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;2ddabd4c.1406) [HAL](https://hal.inria.fr/hal-01052449v4) |
_p_<sup>3</sup> | 593 || 16/08/2016 | Pierrick Gaudry, Aurore Guillevic and François Morain | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;ae418648.1608) |
_p_<sup>3</sup> | 508 || 23/05/2016 | Aurore Guillevic, François Morain and Emmanuel Thomé | [DOI](https://doi.org/10.1007/978-3-319-69453-5_30) |
_p_<sup>3</sup> | 508 || 20/04/2016 | Aurore Guillevic and François Morain | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;ef208f4f.1604) |
_p_<sup>3</sup> | 512 || 02/10/2015 | Razvan Barbulescu, Pierrick Gaudry, Aurore Guillevic and François Morain | [slides](https://webusers.imj-prg.fr/~razvan.barbaud/p3dd52.pdf) |
_p_<sup>3</sup> | 394 || 23/08/2006 | Antoine Joux, Reynald Lercier, Nigel Smart and Frederik Vercauteren | [DOI](https://doi.org/10.1007/11818175_19) |
_p_<sup>4</sup> | 392 || 02/10/2015 | Razvan Barbulescu, Pierrick Gaudry, Aurore Guillevic and François Morain | [slides](http://www.lix.polytechnique.fr/~guillevic/docs/guillevic-catrel15-talk.pdf) |
_p_<sup>5</sup> | 324 || 01/08/2017 | Laurent Grémy, Aurore Guillevic and François Morain | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;68019370.1708) [HAL](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;68019370.1708) |
_p_<sup>6</sup> | 423 || 29/01/2020 | Gary McGuire and Oisín Robinson | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;b28a6ee6.1708) [arxiv](https://arxiv.org/abs/2001.10860) |
_p_<sup>6</sup> | 422 || 16/08/2017 | Laurent Grémy, Aurore Guillevic, François Morain and Emmanuel Thomé | [DOI](https://doi.org/10.1007/978-3-319-72565-9_5) |
_p_<sup>6</sup> | 389 || 16/08/2017 | Laurent Grémy, Aurore Guillevic, François Morain and Emmanuel Thomé | [DOI](https://doi.org/10.1007/978-3-319-72565-9_5) | Pierrick Gaudry and Marion Videau were involved in a first step of the computation (see [DOI](https://doi.org/10.1112/S1461157016000164))
_p_<sup>6</sup> | 300 || 16/08/2017 | Laurent Grémy, Aurore Guillevic, François Morain and Emmanuel Thomé | [PhD thesis](https://tel.archives-ouvertes.fr/tel-01647623) | Announced in [DOI](https://doi.org/10.1007/978-3-319-72565-9_5), Pierrick Gaudry and Marion Videau were involved in a first step of the computation (see [DOI](https://doi.org/10.1112/S1461157016000164))
_p_<sup>6</sup> | 240 || 16/08/2017 | Laurent Grémy, Aurore Guillevic, François Morain and Emmanuel Thomé | [PhD thesis](https://tel.archives-ouvertes.fr/tel-01647623) | Announced in [DOI](https://doi.org/10.1007/978-3-319-72565-9_5), Pierrick Gaudry and Marion Videau were involved in a first step of the computation (see [DOI](https://doi.org/10.1112/S1461157016000164))
_p_<sup>6</sup> | 240 || 02/02/2008 | Pavol Zajac | [PhD thesis](http://www.kaivt.elf.stuba.sk/kaivt/Vyskum/XTRDL?action=AttachFile&do=get&target=ZajacThesis.pdf) |
_p_<sup>12</sup> | 203 || 07/10/2013 | Kenichiro Hayasaka, Kazumaro Aoki, Tetsutaro Kobayashi and Tsuyoshi Takagi | [DOI](http://dx.doi.org/10.1007/978-3-642-42001-6_8) [DOI](http://doi.org/10.14495/jsiaml.6.53) |

field |bits | decimals | Date  | Authors | Link | Comment
:----:|:---:|:---:|:----------:|:--------------------------------------------|:--------:|:--------------------------------------------------
_p_<sup>50</sup> | 1051 || 04/02/2020 | Madhurima Mukhopadhyay, Palash Sarkar, Shashank Singh and Emmanuel Thomé | [ePrint](https://eprint.iacr.org/2020/113) |
_p_<sup>40</sup> |  728 || 29/01/2014 | Palash Sarkar and Shashank Singh | [DOI](https://doi.org/10.1109/TIT.2016.2528996) |
_p_<sup>37</sup> |  592 || 29/01/2014 | Palash Sarkar and Shashank Singh | [DOI](https://doi.org/10.1109/TIT.2016.2528996) |
_p_<sup>57</sup> | 1425 || 06/01/2013 | Antoine Joux | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;8043b0c2.1301) [DOI](http://dx.doi.org/10.1007/978-3-642-38348-9_11) | Kummer extension
_p_<sup>47</sup> | 1175 || 24/12/2012 | Antoine Joux | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;72cb5ee9.1212) [DOI](http://dx.doi.org/10.1007/978-3-642-38348-9_11) | Kummer extension
_p_<sup>30</sup> |  556 || 09/11/2005 | Antoine Joux and Reynald Lercier | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;590b8b16.0511) | Kummer extension
_p_<sup>25</sup> |  401 || 24/10/2005 | Antoine Joux and Reynald Lercier | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;ff262658.0510) [DOI](https://https//doi.org/10.1007/11761679_16) |
_p_<sup>18</sup> |  334 || 28/06/2005 | Reynald Lercier and Frederik Vercauteren | [nmbrthy](https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;8e78be2e.0506) [paper](http://homes.esat.kuleuven.be/~fvercaut/dlog/100.pdf) |

### Choosing key-sizes

See this page [Pairing-friendly curves](https://members.loria.fr/AGuillevic/pairing-friendly-curves/)
for a short-list of interesting curves for cryptography.


# How to stay up-to-date (_veille technologique_)

## ANSSI, NIST
In France, ANSSI is in charge of recommendations
[web site](https://www.ssi.gouv.fr/)
and publishes a report named RGS (Référentiel Général de Sécurité).
RGS-B1 is about cryptographic key sizes
[link to latest version: 2021](https://cyber.gouv.fr/sites/default/files/2021/03/anssi-guide-mecanismes_crypto-2.04.pdf)
([previous version: 2014](https://cyber.gouv.fr/sites/default/files/2022-10/RGS_v-2-0_B1.pdf)).
In the US, for commercial and non-critical civil use, NIST (National Institute of
Standards and Technology) publishes standards, organizes competitions to decide
of new standards.

## IACR events and publications

IACR (International Association for Cryptologic Research) conferences and workshops: all past talks are available on the Youtube
channel of IACR. In particular, [RWC](https://rwc.iacr.org/) (Real
Wold Crypto) is a more applied conference, with many accessible talks (not too
technical).
Published papers older than two years are freely available at [IACR archive](https://www.iacr.org/archive/).

Watch out: the online archive [ePrint](https://eprint.iacr.org/)
hosts non-reviewed _draft_ papers. It means that in some cases, the results or
the claims are wrongs (this is more a platform for hosting discussions).
To ensure that the work was peer-reviewed (other specialists of the field
checked the technical content) and approved, check if the paper is
_published_ at a workshop or conference. It should have a mention on the
paper, usually a footnote on the first page.

## Blogs

-   Pr. Matthew Green [blog](https://blog.cryptographyengineering.com/),
    _A few thoughts on cryptographic engineering_.
-   Pr. Steven Galbraith, dedicated to elliptic
    curves [blog](https://ellipticnews.wordpress.com/)
-   [COSIC blog](https://www.esat.kuleuven.be/cosic/blog/),
    research group at KU leuven, Belgium, in particular,
	[a post on factoring](https://www.esat.kuleuven.be/cosic/blog/factoring1/)

## Mailing-lists

-   Mailing-list `curves@moderncrypto.org` on applied cryptography related to
    elliptic curves. Archives at [moderncrypto.org](https://moderncrypto.org)

## Mathematical software

To experiment ``by hand'' simple cases of RSA for example, one needs a software
able to handle large-precision integers. Indeed, native integers are only 64-bit
long on a processor.

-   **SageMath** is a free mathematical software whose
    interface has a Python-like syntax, and there is a wide free documentation, in
    English and in French (Zimmermann's book).
-   **PARI-GP** is an alternative, based on a C library, with a dedicated language
    (GP).
-   For development, the **GMP** library is the reference in C language for very
    efficient multi-precision arithmetic.

## Cours au Collège de France

With a different approach, the lectures at _Collège de France_ are recorded
and posted online. There is a department on computer science. Usually there are
lectures targeting a very general audience, and also more dedicated lectures,
for researchers (less accessible).
About cybersecurity, there was a talk by Guillaume Poupard (head of ANSSI) in 2019:
[link]https://www.college-de-france.fr/site/gerard-berry/seminar-2019-02-13-17h30.htm)
(also available in audio-only format on
[_France Culture_ website](https://www.franceculture.fr/emissions/series/ou-va-linformatique)).

## Further reading

### PhD theses
Usually, PhD theses are good documents where to start reading about a new topic.
Good PhD theses start with a long introduction on the topic of the thesis,
assuming general knowledge (undergraduate, Master-1 level) but not dedicated
knowledge of the field.
To know about on-going theses in France: [theses.fr](http://theses.fr/).
Searching PDF documents: [Theses En Ligne](https://tel.archives-ouvertes.fr/).

### Historical books
Some historical readings: big bookstores in UK even have ``Bletchley Park''
dedicated shelves.

# Index, English-French

batch gcd: calcul de pgcd par paquets, consiste à multiplier entre eux les nombres avant de calculer des pgcd  
cado-nfs: logiciel développé à Nancy pour la factorisation et le logarithme discret  
DH (Diffie-Hellman): nom du cryptosystème publié en 1976  
discrete logarithm: logarithme discret  
finite field: corps fini  
gcd (greatest common divisor): pgcd  
key: clé  
lsb, least significant bits: bits de poids faible  
modulus: module  
msb, most significant bits: bits de poids fort  
number field: corps de nombres (par exemple **Q**(i) avec i<sup>2</sup> = -1)  
padding: remplissage  
ransomware: rançongiciel  
RSA (Rivest Shamir Adleman): nom du cryptosystème publié en 1977  
smooth, _B_-smooth: friable, pour un nombre entier, se factorise en produit de nombres premiers inférieurs à une borne _B_  


## Notations

**Z**/_n_**Z**: the ring of integers modulo _n_  
**Z**/_p_**Z**, **F**<sub>_p_</sub>, GF(_p_): the prime field of _p_ elements, where _p_ is prime  
GF(2<sup>_n_</sup>): the finite field of 2<sup>_n_</sup> elements, requires a choice
  of an irreducible polynomial of degree _n_ to represent it, for example
  _f_(_x_) = _x_<sup>127</sup> + _x_ + 1 for _n_ = 127
