
# classical L_N(alpha,c) function

def L_N_alpha_c(log2N, alpha, c):
    """return log_2 (L_N(c,alpha)) """
    lnN = float(log(2))*float(log2N)
    return float(c) * log2N**float(alpha) * float(log(lnN,2))**(float(1-alpha))

alpha = 1/3
c = (64/9)^(1/3)
cs = (32/9)^(1/3)

floor(L_N_alpha_c(2538, alpha, c)) == 128 and ceil(L_N_alpha_c(2537, alpha, c)) == 128

# DL-768 Kleinjung et al was made in 2017. 5300 core-years, 2.2 GHz Intel Xeon E5-2660
cost_DL_768 = float(RR(log(5300*365.25*24*60*60*2.2*10**9,2)))
# 68.32
theoretical_cost_NFS_768 = L_N_alpha_c(768, 1/3, (64/9)^(1/3))
# 76.51
delta_DL_768 = theoretical_cost_NFS_768 - cost_DL_768
print("cost_DL_768 = 2^{:.4f} delta_DL_768 = {:.4f} bits".format(cost_DL_768, delta_DL_768))
# 8.19 -> 8.2

# RSA-768 https://eprint.iacr.org/2010/006.pdf
# polyselect 0.5*80 = 40 core-year
# sieving single core 2.2 GHz AMD Opteron 1500 core-years
# linear algebra 98 days * 576 cores on  2.2GHz dual hex-coreAMD processors with Infiniband at EPFL
# total: 40 + 1500  core-years + 56448 core-days ~= 1694.5 core-years
cost_IF_768 = float(RR(log(((40+1500)*365.25 + 98*576)*24*60*60*2.2*10**9,2)))
delta_RSA_768 = theoretical_cost_NFS_768 - cost_IF_768
print("cost_IF_768 = 2^{:.4f} delta_RSA_768 = {:.4f} bits".format(cost_IF_768, delta_RSA_768))

# RSA-240 is 795-bit long
theoretical_cost_NFS_240 = L_N_alpha_c(795, 1/3, (64/9)^(1/3))
# Intel Xeon Gold 6130 CPUs with 16 physical cores (32 hyperthreaded cores) running at 2.10 GHz
# RSA-240
core_years_RSA_240 = 76 + 794 + 83
cost_IF_240 = float(RR(log(core_years_RSA_240 *365.25*24*60*60*2.1*10**9,2)))

delta_RSA_240 = theoretical_cost_NFS_240 - cost_IF_240
print("cost_IF_240 = 2^{:.4f} delta_RSA_240 = {:.4f} bits".format(cost_IF_240, delta_RSA_240))

core_years_DL_240 = 152 + 2400 + 625
cost_DL_240 = float(RR(log(core_years_DL_240 *365.25*24*60*60*2.1*10**9,2)))
delta_DL_240 = theoretical_cost_NFS_240 - cost_DL_240
print("cost_DL_240 = 2^{:.4f} delta_DL_240 = {:.4f} bits".format(cost_DL_240, delta_DL_240))

# DL-240

# cost SNFS <-> Hidden SNFS-1024 385 core-years 240 + 123 + 13 + 9 on a Intel Xeon E5-2650 at 2.00 GHz
p_SNFS = 163323987240443679101402070093049155030989439806917519173580070791569227728932850358498862854399351423733697660534800194492724828721314980248259450358792069235991826588944200440687094136669506349093691768902440555341493237296555254247379422702221515929837629813600812082006124038089463610239236157651252180491
freq_proc_SDL_1024 = 2.00
core_years_SDL_1024 = 385
cost_SDL_1024 = float(RR(log(core_years_SDL_1024*365.25*24*60*60*freq_proc_SDL_1024*10**9,2))) # 64.4
#theoretical_cost_SDL_1024 = RR(L_N_alpha_c(log(p_SNFS,2), 1/3, (32/9)^(1/3)))
theoretical_cost_SDL_1024 = float(RR(L_N_alpha_c(1024, 1/3, cs)))
delta_SDL_1024 = theoretical_cost_SDL_1024 - cost_SDL_1024
# 4.46

print floor(L_N_alpha_c(2940, alpha, c) - delta_DL_768) == 128 and ceil(L_N_alpha_c(2939, alpha, c) - delta_DL_768) == 128
print floor(L_N_alpha_c(7423, alpha, c) - delta_DL_768) == 192 and ceil(L_N_alpha_c(7422, alpha, c) - delta_DL_768) == 192

print floor(L_N_alpha_c(3025, alpha, c) - delta_RSA_768) == 128 and ceil(L_N_alpha_c(3024, alpha, c) - delta_RSA_768) == 128
print floor(L_N_alpha_c(7572, alpha, c) - delta_RSA_768) == 192 and ceil(L_N_alpha_c(7571, alpha, c) - delta_RSA_768) == 192

print floor(L_N_alpha_c(3135, alpha, c) - delta_RSA_240) == 128 and ceil(L_N_alpha_c(3134, alpha, c) - delta_RSA_240) == 128
print floor(L_N_alpha_c(7762, alpha, c) - delta_RSA_240) == 192 and ceil(L_N_alpha_c(7761, alpha, c) - delta_RSA_240) == 192

print floor(L_N_alpha_c(3043, alpha, c) - delta_DL_240) == 128 and ceil(L_N_alpha_c(3042, alpha, c) - delta_DL_240) == 128
print floor(L_N_alpha_c(7603, alpha, c) - delta_DL_240) == 192 and ceil(L_N_alpha_c(7602, alpha, c) - delta_DL_240) == 192

print floor(L_N_alpha_c(4781, alpha, cs) - delta_SDL_1024) == 128 and ceil(L_N_alpha_c(4780, alpha, cs) - delta_SDL_1024) == 128
print floor(L_N_alpha_c(12472, alpha, cs) - delta_SDL_1024) == 192 and ceil(L_N_alpha_c(12471, alpha, cs) - delta_SDL_1024) == 192
print floor(L_N_alpha_c(25056, alpha, cs) - delta_SDL_1024) == 256 and ceil(L_N_alpha_c(25055, alpha, cs) - delta_SDL_1024) == 256

def print_LN_RSA_DL():
    alpha = 1/3
    c = (64/9)^(1/3)
    cs= (32/9)^(1/3)
    print("log2N log2N/2 q.sieve   L_N     RSA     DL       SDL")
    for log2N in range(64, 1024*8+1, 8):
        generic_DL = log2N//2
        quadratic_sieve = L_N_alpha_c(log2N, 1/2, sqrt(2))
        LQ = L_N_alpha_c(log2N, alpha, c)
        LQS = L_N_alpha_c(log2N, alpha, cs)
        print("{:5d} {:5d}   {:7.3f}  {:7.3f} {:7.3f} {:7.3f} {:7.3f}".format(log2N, generic_DL, quadratic_sieve, LQ, LQ-delta_RSA_240, LQ-delta_DL_240, LQS-delta_SDL_1024))

def print_LN_STNFS():
    alpha=1/3
    c0 = (96/9)^(1/3)
    c1 = (48/9)^(1/3)
    c2 = (32/9)^(1/3)
    print("log2N L_N96 L_N48 L_N32")
    for log2N in range(64, 12288+1, 64):
        LQ0 = L_N_alpha_c(log2N, alpha, c0)
        LQ1 = L_N_alpha_c(log2N, alpha, c1)
        LQ2 = L_N_alpha_c(log2N, alpha, c2)
        print("{:4d} {:7.3f} {:7.3f} {:7.3f}".format(log2N, LQ0, LQ1, LQ2))

def print_year():
    alpha=1/3
    c = (64/9)^(1/3)
    m0 = 2010 - 3/2*L_N_alpha_c(768, alpha, c)
    print("log2N  year")
    for log2N in range(64, 1280+1, 64):
        LQ = L_N_alpha_c(log2N, alpha, c)
        m = 3/2 * LQ + m0
        print("{:4d} {:.2f}".format(log2N, float(m)))
