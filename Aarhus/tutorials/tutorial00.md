---
pagetitle: "Elliptic Curves, Number Theory and Cryptography, Tutorial 0"
author: 'Aurore Guillevic (Inria Nancy Grand-Est)'
date: 'February 3, 2022'
title: 'Introduction to SageMath'
institute: 'Aarhus University'
---

# Introduction to SageMath

  SageMath library: a mathematical software suite based on Python,
  open-source.

## Installation

Install SageMath from [](https://www.sagemath.org/download.html)

**Note**: you cannot move SageMath somewhere else on your disk once it is
installed.

If the installation is successful, the `sage` command should give a prompt:
```shell
sage:
```
You can test it with a simple command:
```python
sage: 42.factor()
2 * 3 * 7
```

On Linux, to run `sage` from everywhere: add the SageMath directory
into your PATH, or in your `.bash_aliases`, create an alias to your installation:
```shell
alias sage='~/Libs/SageMath/sage'
```

## SageMath Tutorial

https://doc.sagemath.org/html/en/tutorial/index.html

https://doc.sagemath.org/html/en/tutorial/introduction.html

## Quick presentation of SageMath

(From Cyril Bouvier's tutorial)

### Sage

Sage is an open-source computer algebra software.

It is based on Python 3 (from 2020).

It combines lots of open-source libraries (GMP, Pari, FLINT, NTL,
MPFR, FFLAS-FFPACK, GMP-ECM, ...)

Four ways to use it:

-   interactive shell (via IPython);
-   script files;
-   notebook (via Jupyter);
-   python module.

### Running Sage

#### Interactive Shell

```shell
sage
```

It opens an interactive prompt (using IPython).
You can write commands and press Enter to run them; the result is printed
below.
```python
sage:~59-17
42
```

#### Script files

Write commands in a file and run it with
```shell
sage scriptname.sage
```
Sage translates the file `scriptname.sage` into a Python file and
executes it.
```shell
echo "print(59-17)" > example.sage
sage example.sage
```
Prints on the terminal:
```python
42
```
#### Notebook
Similar to the interactive shell but you can use it in your browser.
```shell
sage --notebook
```

#### Python module

Follow the instructions at https://wiki.sagemath.org/SpyderInstructions
to configure Spyder.

You can import Sage as a Python library into a Python code with
```python
from sage.all_cmdline import *   # import sage library
```
or alternatively
```python
from sage.all import *   # import sage library
```
Then run it with the Python of Sage (without the extension `.py`):
```shell
sage -python myfile
```
If this is a module file in a directory `myproject/test/mytest.py`:
```shell
sage -python -m myproject.test.mytest
sage -python -m myproject.test.mytest <customized command-line options>
```

## Python

**Dynamically type**: types of variables are verified at runtime.  
**Variable name**: can contains letters, numbers and underscore `_`; cannot start
  with a number; case sensitive.
**Blocks**: whitespace indentation (or tabulations) are used to delimit
  code blocks.
**Scope of variables**: a block.
**Comments**: Everything after a `#` is a comment. 

### Python - types

#### Built-in types

-   `b = True` boolean, `bool`, can be `True` or `False`
-   `n = 42` integer, `int`
-   `f = 42.17e14` floating-point number, `float`
-   `s = "Hello World!"`  string, `str`
-   `L = [42, "ABC", 17.14]` list, `list`  
    `L[i]` to get the (i+1)-th element,  
	`len(L)` for its length,  
	from `L[0]` to `L[len(l)-1]`
-   `t = (42, "ABC", 17.14)` tuple  
    `t[i]` to get the element at position `i`
-   `S = {42, "ABC", 17.14}` set
-   `D = {"answer": 42, "n": 17, 14: (2,7)}` dictionary, `dict`  
    `D[key]` to get the element corresponding to the key `key`, for example
	`D["answer"]`, `D["n"]`, `D[14]`

#### Python - conditionals

```python
if condition1:
    ... # do something if condition1 is true
elif condition2:
    ... # do something else if condition 1 is false but condition2 is true
elif condition3:
    ... # do something else if condition3 is true but others are false
else:
    ... # do something if no condition is true
```
The `else` and `elif` clauses are optional. The `elif` clause can be
repeated as many times as necessary.

One can use `and`, `or` and `not` to combine conditions.

```python
if myvariable is None:
    ... # do something if the variable is None (not defined)
if p.is_prime():
    Fp = GF(p) # define a prime finite field if p is a prime
```

#### Python - for loop, while loop
```python
for i in range(10):
    print(i**2)

i = 0
while i < 10:
    print(i**2)
	i = i+1
```
#### Functions, methods

One defines a function with `def`, with input variables, and a return statement.
If there is no `return`, then by default the function returns the special type `None`.
```python
def sum_up_to(n):
    s = 0
    for i in range(0, n):
        s = s + i
    return s
```
A **method** is attached to an instance of a **class**. This is a function
defined inside a class, whose 1st argument is `self`. To use it on Sage
predefined classes:
```python
s = "Hello world"
print(s.capitalize())
print(s.split(" "))
```
gives
```python
Hello world
['Hello', 'world']
```

## Sage - Differences with Python

Syntactic sugar available:
```python
QQx.<x> = QQ[] # defines a polynomial ring in the variable x over the rationals QQ
f = x^3
3^3  # in Sage ^ is powering
3**3 # python powering is still defined in Sage
```
Check at https://doc.sagemath.org/html/en/tutorial/tour_assignment.html for a
longer list of arithmetic with SageMath.

The division operation `42/17` gives 

-  the floating-point number ` 2.4705882352941178` in Python3
-  the rational 42/17 (in **Q**) in SageMath

## From Python to SageMath and vice-versa

### Important features of the interactive shell of Sage.

A list of methods attached to a variable can be printed by pressing the Tab key:
```python
E = EllipticCurve([0,-2])
E. # here press tab right after the dot
```
The documentation can be printed by appending a `?' to variables, functions
and methods. 

The **code** can be printed by appending a double question mark `??` to
functions and methods.

### Remove the syntactic sugar from SageMath interpreter

```python
preparse("QQx.<x> = QQ[]")
```

### Ask which packages to import in a python file

```python
import_statements(QQ)
```
answers
```python
# ** Warning **: several names for that object: Q, QQ
from sage.rings.rational_field import Q
```

### History

Print the history of all commands you typed in the interpreter:
```python
%hist
```
Then you can copy-paste in a file to save your work.

### Files `.sage` and `.py`

For small SageMath scripts: files `myfile.sage` with `.sage` extension, no
`import` needed, the interactive shell (like `ipython`) imports everything.

Run this file:
```python
load("myfile.sage")
```
within SageMath interactive shell.  
This creates a file `myfile.sage.py` which is a translation into Python of the
SageMath script. It removes the syntactic sugar, and add all the needed `import`
statements.

To directly write Python-like code: create a file with `.py` extension such as `myfile.py`.

### Run SageMath within Spyder

Launch Spyder, then open your file with Spider.

## Alternatively, uses a Text Editor

-  `Kate` on linux (or maybe you already know vim, emacs etc)
-  `notepad++` on Windows, see https://notepad-plus-plus.org/downloads/
-  Any suggestions for MacOS?
