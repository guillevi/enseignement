# Tutorials for Elliptic Curves, Number Theory and Cryptography

**Spring 2022: Every Thursdays, 3pm-5pm.**

0. [Tutorial 0: Introduction to SageMath](./tutorials/tutorial00.md)
1. [Tutorial 1: Programming Bachet replication formula in Python and SageMath](./tutorials/tutorial01.md)
2. Tutorial 2 to come: models of elliptic curves (projective, Jacobian,
   modified Jacobian), and scalar multiplication algorithms.
